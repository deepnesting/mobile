import 'dart:async';
import 'dart:io';

import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class NestRepository {
  WebClient webClient = WebClient();
  NestRepository();

  Future fetchOffers(
      {CancelToken cancelToken,
      List<int> offersIds,
      int priceFrom,
      int priceTo,
      int searchType,
      int buildType,
      double mapZoom,
      int userId,
      List<int> statuses}) async {
    return await webClient.fetchOffers(
      cancelToken: cancelToken,
      offersIds: offersIds,
      priceFrom: priceFrom,
      priceTo: priceTo,
      searchType: searchType,
      buildType: buildType,
      mapZoom: mapZoom,
      userId: userId,
      statuses: statuses,
    );
  }

  Future fetchOfferContacts({
    @required int offerId,
  }) async {
    return await webClient.fetchOfferContacts(offerId: offerId);
  }

  Future<User> register(
      {@required String login, @required String password}) async {
    return await webClient.register(login: login, password: password);
  }

  Future<User> login(
      {@required String login, @required String password}) async {
    return await webClient.login(login: login, password: password);
  }

  Future logout() async {
    SharedPreferencesUtils.setVariableToShared(title: 'Login', variable: '');
    SharedPreferencesUtils.setVariableToShared(title: 'Password', variable: '');
    SharedPreferencesUtils.setVariableToShared(title: 'Token', variable: '');
  }

  Future<UserMe> getUserInfo() async {
    return await webClient.getUserInfo();
  }

  Future updateUserInfo(
      {String firstName, String lastName, String avatar}) async {
    return await webClient.updateUserInfo(
        firstName: firstName, lastName: lastName, avatar: avatar);
  }

  Future updateUserProfileData({
    @required String bDate,
    @required String homeTown,
    @required String info,
  }) async {
    return webClient.updateUserProfileData(
      bDate: bDate,
      homeTown: homeTown,
      info: info,
    );
  }

  Future createOffer({
    @required int searchType,
    @required int buildType,
    @required int roomCount,
    @required String comment,
    @required int price,
    @required String address,
    List<ImageModel> images,
  }) async {
    return await webClient.createOffer(
      searchType: searchType,
      buildType: buildType,
      roomCount: roomCount,
      comment: comment,
      price: price,
      address: address,
      images: images,
    );
  }

  Future<ImageModel> uploadPhoto({@required File photo}) async {
    return await webClient.uploadPhoto(photo: photo);
  }

  Future fetchSingleOffer({
    @required offerId,
  }) async {
    return await webClient.fetchSingleOffer(offerId: offerId);
  }

  Future<List<int>> getCheckedOffersFromDB() async {
    List<int> checkedOffers = List();
    List<CheckedOffer> checkedOffersList =
        await CheckedOffer().select().toList();
    for (int i = 0; i < checkedOffersList.length; i++) {
      checkedOffers.add(checkedOffersList[i].offerId);
    }
    return checkedOffers;
  }

  Future saveCheckedOffersList({@required List<int> checkedOffers}) async {
    for (int i = 0; i < checkedOffers.length; i++) {
      await CheckedOffer(offerId: checkedOffers[i]).save();
    }
  }

  Future<List<int>> getFavoriteOffersFromDB() async {
    List<int> favoriteOffers = List();
    List<FavoriteOffer> favoriteOffersList =
        await FavoriteOffer().select().toList();
    for (int i = 0; i < favoriteOffersList.length; i++) {
      favoriteOffers.add(favoriteOffersList[i].offerId);
    }
    return favoriteOffers;
  }

  Future saveFavoriteOffersList({@required List<int> favoriteOffers}) async {
    //delete all favorites before saving
    FavoriteOffer().select().delete();
    for (int i = 0; i < favoriteOffers.length; i++) {
      await FavoriteOffer(offerId: favoriteOffers[i]).save();
    }
  }

  Future<List<OfferCommentsModel>> fetchOfferComments(
      {@required int offerId}) async {
    return await webClient.fetchOfferComments(offerId: offerId);
  }

  Future sendComment({@required String comment, @required int offerId}) async {
    return await webClient.sendComment(comment: comment, offerId: offerId);
  }

  Future<SearchTabEnum> getLastSearchTab() async {
    String tab =
        await SharedPreferencesUtils.getVariableFromShared(title: 'searchTab');
    switch (tab) {
      case 'map':
        return SearchTabEnum.map;
        break;
      case 'home':
        return SearchTabEnum.home;
        break;
      default:
        return SearchTabEnum.map;
        break;
    }
  }

  Future setSearchTab({@required SearchTabEnum searchTabEnum}) async {
    String tab;
    switch (searchTabEnum) {
      case SearchTabEnum.map:
        tab = 'map';
        break;
      case SearchTabEnum.home:
        tab = 'home';
        break;
    }
    await SharedPreferencesUtils.setVariableToShared(
        variable: tab, title: 'searchTab');
  }

  Future<bool> ifMapDisabled() async {
    return await webClient.ifMapDisabled();
  }

  Future submitForModeration({@required int offerId}) async {
    return await webClient.submitForModeration(offerId: offerId);
  }

  Future unPublishOffer({@required int offerId}) async {
    return await webClient.unPublishOffer(offerId: offerId);
  }

  Future submitOffer({@required int offerId}) async {
    return await webClient.submitOffer(offerId: offerId);
  }
}
