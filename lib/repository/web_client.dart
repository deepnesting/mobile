import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/models/offer_contacts_model.dart';
import 'package:deepnesting/models/point_model.dart';
import 'package:deepnesting/utils/utils.dart';

class WebClient {
  Dio dio = Dio();
  int requestID = 0;
  WebClient();

  final String url = 'https://m.ugnest.com/rpc/v1';

  Future fetchOffers({
    CancelToken cancelToken,
    List<int> offersIds,
    int priceFrom,
    int priceTo,
    int searchType,
    int buildType,
    double mapZoom,
    int userId,
    List<int> statuses,
  }) async {
    int id = requestID++;
    //print('request with token $token');
    var response;
    response = await dio.post(
      '$url',
      cancelToken: cancelToken,
      data: {
        "id": id,
        "jsonrpc": "2.0",
        "method": "offers.FastCards",
        "params": {
          "isMap": true,
          "search": {
            "zoom": mapZoom,
            "ids": offersIds,
            "priceFrom": priceFrom,
            "priceTo": priceTo,
            "searchType": searchType,
            "buildType": buildType,
            "userId": userId,
            "statuses": statuses
          }
        }
      },
    );
    if (cancelToken != null) {
      if (cancelToken.isCancelled) {
        print('token $id cancelled, return');
        return null;
      } else {
        print('token $id not cancelled');
      }
    }
    //print('token $token state after request');
    if (response.statusCode == 200) {
      var resultOffers = response.data['result']['offers'];
      List<OfferModel> offersList = resultOffers
          .map<OfferModel>((json) => OfferModel.fromJson(json))
          .toList();

      if (cancelToken != null) {
        if (cancelToken.isCancelled) {
          return null;
        }
      }

      var resultPoints = response.data['result']['points'];
      List<PointModel> pointsList = List();
      if (resultPoints != null) {
        pointsList = resultPoints
            .map<PointModel>((json) => PointModel.fromJson(json))
            .toList();
      }

      if (cancelToken != null) {
        if (cancelToken.isCancelled) {
          return null;
        }
      }

      return [
        offersList,
        pointsList,
      ];
    } else {
      return throw ('Ошибка сети');
    }
  }

  Future fetchSingleOffer({
    @required offerId,
  }) async {
    var response;
    response = await dio.post(
      '$url',
      data: {
        "id": "970959c5-6ffc-4bdf-8748-1461da939947",
        "jsonrpc": "2.0",
        "method": "offers.GetFast",
        "params": {"offerId": offerId}
      },
    );
    if (response.statusCode == 200) {
      return OfferSingleModel.fromJson(response.data['result']);
    } else {
      return throw ('Ошибка сети');
    }
  }

  Future fetchOfferContacts({
    @required int offerId,
  }) async {
    final response = await dio.post('$url', data: {
      "id": "4baa899d-22c0-44bc-96bb-ae2a958cd970",
      "jsonrpc": "2.0",
      "method": "offers.Contacts",
      "params": {"offerID": offerId}
    });
    if (response.statusCode == 200) {
      var resultContacts = response.data['result'];
      List<OfferContactsModel> contactsList = resultContacts
          .map<OfferContactsModel>((json) => OfferContactsModel.fromJson(json))
          .toList();
      return contactsList;
    } else {
      return throw ('Ошибка сети');
    }
  }

  Future<User> register(
      {@required String login, @required String password}) async {
    final response = await dio.post('$url', data: {
      "id": "368171e7-a95e-444b-a029-f5fe0e564639",
      "jsonrpc": "2.0",
      "method": "auth.Reg",
      "params": {"phone": "$login", "password": "$password", "sessionId": ""}
    });
    try {
      User user = User.fromJson(response.data['result']);
      SharedPreferencesUtils.setVariableToShared(
          title: 'Login', variable: login);
      SharedPreferencesUtils.setVariableToShared(
          title: 'Password', variable: password);
      SharedPreferencesUtils.setVariableToShared(
          title: 'Token', variable: user.token);
      return user;
    } catch (e) {
      switch (response.data['error']['message']) {
        case 'weak password':
          throw ('Слишком слабый пароль');
        case 'login already taken':
          throw ('Логин занят');
        case 'bad code':
          throw ('Неподходящий логин');
        default:
          throw ('Ошибка сети');
      }
    }
  }

  Future<User> login(
      {@required String login, @required String password}) async {
    final response = await dio.post('$url', data: {
      "id": "89863c4a-ee6c-4ed3-bbf9-ad0143f1e72c",
      "jsonrpc": "2.0",
      "method": "auth.Login",
      "params": {"phone": "$login", "password": "$password", "sessionId": ""}
    });
    try {
      User usr = User.fromJson(response.data['result']);
      SharedPreferencesUtils.setVariableToShared(
          title: 'Login', variable: login);
      SharedPreferencesUtils.setVariableToShared(
          title: 'Password', variable: password);
      SharedPreferencesUtils.setVariableToShared(
          title: 'Token', variable: usr.token);
      return usr;
    } catch (e) {
      switch (response.data['error']['message']) {
        case 'wrong password':
          throw ('Неверный пароль');
        default:
          throw ('Ошибка сети');
      }
    }
  }

  Future<UserMe> getUserInfo() async {
    String token =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Token');

    var _dio = Dio();
    _dio.interceptors.clear();
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      options.headers["Authorization"] = "Bearer " + token;
      return options;
    }));

    final response = await _dio.post('$url', data: {
      "id": "63975845-8d29-4147-96bf-ca2078d84655",
      "jsonrpc": "2.0",
      "method": "users.Me",
      "params": {}
    });

    if (response.statusCode == 200) {
      try {
        return UserMe.fromJson(response.data['result']);
      } catch (e) {
        print('user info error: $e');
        throw ('Ошибка сети');
      }
    } else {
      throw ('Ошибка сети');
    }
  }

  Future updateUserInfo(
      {String firstName, String lastName, String avatar}) async {
    String token =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Token');

    var _dio = Dio();
    _dio.interceptors.clear();
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      options.headers["Authorization"] = "Bearer " + token;
      return options;
    }));

    final response = await _dio.post('$url', data: {
      "id": "289ae50a-bd99-43f7-9d40-28cd56d68c47",
      "jsonrpc": "2.0",
      "method": "users.Update",
      "params": {
        "user": {
          "lastName": "$lastName",
          "firstName": "$firstName",
          "avatarUrl": "$avatar"
        }
      }
    });

    if (response.statusCode != 200) {
      throw ('Ошибка сети');
    }
  }

  // add user's details
  Future updateUserProfileData({
    String bDate,
    String homeTown,
    String info,
  }) async {
    String token =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Token');

    var _dio = Dio();
    _dio.interceptors.clear();
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      options.headers["Authorization"] = "Bearer " + token;
      return options;
    }));

    final response = await _dio.post('$url', data: {
      "id": "ec68bb78-5c9c-4034-bd90-08accbdbd8b1",
      "jsonrpc": "2.0",
      "method": "users.Attributes",
      "params": {
        "user": {"bdate": "$bDate", "htown": "$homeTown", "info": "$info"}
      }
    });

    if (response.statusCode != 200) {
      throw ('Ошибка сети');
    }
  }

  Future createOffer({
    @required int searchType,
    @required int buildType,
    @required int roomCount,
    @required String comment,
    @required int price,
    @required String address,
    List<ImageModel> images,
  }) async {
    String token =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Token');

    var _dio = Dio();
    _dio.interceptors.clear();
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      options.headers["Authorization"] = "Bearer " + token;
      return options;
    }));

    final response = await _dio.post('$url', data: {
      "id": "2bde735a-11d2-4616-b3a7-3a29ec779e41",
      "jsonrpc": "2.0",
      "method": "offers.FastCreate",
      "params": {
        "offerRequest": {
          "address": address,
          "buildType": buildType,
          "comment": comment,
          "rentDuration": 3,
          "roomCount": roomCount,
          "searchType": searchType,
          "price": price,
          "city": 1,
          "images": images
        }
      }
    });

    try {
      return response.data['result'];
    } catch (e) {
      throw (response.data['error']['message']);
    }
  }

  /*Future<ImageModel> uploadPhoto({@required File photo}) async {
    FormData formData = FormData.fromMap({
      "file": MultipartFile.fromFileSync(photo.path, filename: photo.path),
    });

    try {
      final response =
          await dio.post('https://m.ugnest.com/api/v1/upload', data: formData);
      return ImageModel.fromJson(response.data);
    } catch (e) {
      print('error on upload photo ${e.toString()}');
      throw (e.toString());
    }
  }*/

  Future<ImageModel> uploadPhoto({@required File photo}) async {
    var request = http.MultipartRequest(
        "POST", Uri.parse('https://m.ugnest.com/api/v1/upload'));
    var multipartFile = await http.MultipartFile.fromPath("file", photo.path,
        contentType: MediaType('image', 'jpeg'));
    request.files.add(multipartFile);
    http.StreamedResponse response = await request.send();
    var responseByteArray = await response.stream.toBytes();

    return ImageModel.fromJson(json.decode(utf8.decode(responseByteArray)));
  }

  Future<List<OfferCommentsModel>> fetchOfferComments({
    @required int offerId,
  }) async {
    final response = await dio.post('$url', data: {
      "id": "07edc0d5-6885-4883-b7d1-a426452e7804",
      "jsonrpc": "2.0",
      "method": "comments.OfferComments",
      "params": {"offerID": offerId}
    });
    if (response.statusCode == 200) {
      var resultComments = response.data['result'];
      List<OfferCommentsModel> offerCommentsList = resultComments
          .map<OfferCommentsModel>((json) => OfferCommentsModel.fromJson(json))
          .toList();
      return offerCommentsList;
    } else {
      return throw ('Ошибка сети');
    }
  }

  Future<int> sendComment(
      {@required String comment, @required int offerId}) async {
    String token =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Token');

    var _dio = Dio();
    _dio.interceptors.clear();
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      options.headers["Authorization"] = "Bearer " + token;
      return options;
    }));

    final response = await _dio.post('$url', data: {
      "id": "15f035e1-1ebd-4037-8bda-bfde02b4236f",
      "jsonrpc": "2.0",
      "method": "comments.AddOfferComment",
      "params": {"offerID": offerId, "comment": comment}
    });

    try {
      return response.data['result'];
    } catch (e) {
      if (response.data['error']['code'] == 512) {
        throw ('Необходимо зарегистрироваться перед отправкой комментария');
      }
      throw (response.data['error']['message']);
    }
  }

  Future<bool> ifMapDisabled() async {
    final response = await dio.post('$url', data: {
      "id": "ff5e54c2-5e84-49ee-8c4e-9db319bc4d9e",
      "jsonrpc": "2.0",
      "method": "app.Settings",
      "params": {}
    });
    if (response.statusCode == 200) {
      try {
        return response.data['result']['disableMap'];
      } catch (e) {
        return throw (e);
      }
    } else {
      return throw ('Ошибка сети');
    }
  }

  // it seems that the method should return null in case of success
  Future submitForModeration({@required int offerId}) async {
    String token =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Token');

    var _dio = Dio();
    _dio.interceptors.clear();
    _dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options) {
          options.headers["Authorization"] = "Bearer " + token;
          return options;
        },
      ),
    );

    final response = await _dio.post(
      '$url',
      data: {
        "id": "884e70e7-9659-4264-8ce6-8dc248be50eb",
        "jsonrpc": "2.0",
        "method": "offers.SubmitForModeration",
        "params": {"offerID": offerId}
      },
    );

    try {
      return response.data['result'];
    } catch (e) {
      if (response.data['error']['code'] == -32603) {
        throw ('Необходимо войти в учетную запись');
      }
      throw (response.data['error']['message']);
    }
  }

  Future unPublishOffer({@required int offerId}) async {
    String token =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Token');

    var _dio = Dio();
    _dio.interceptors.clear();
    _dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options) {
          options.headers["Authorization"] = "Bearer " + token;
          return options;
        },
      ),
    );

    final response = await _dio.post(
      '$url',
      data: {
        "id": "884e70e7-9659-4264-8ce6-8dc248be50eb",
        "jsonrpc": "2.0",
        "method": "offers.UnPublish",
        "params": {"offerID": offerId}
      },
    );

    try {
      return response.data['result'];
    } catch (e) {
      if (response.data['error']['code'] == -32603) {
        throw ('Необходимо войти в учетную запись');
      }
      throw (response.data['error']['message']);
    }
  }

  Future submitOffer({@required int offerId}) async {
    String token =
        await SharedPreferencesUtils.getVariableFromShared(title: 'Token');

    var _dio = Dio();
    _dio.interceptors.clear();
    _dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options) {
          options.headers["Authorization"] = "Bearer " + token;
          return options;
        },
      ),
    );

    final response = await _dio.post(
      '$url',
      data: {
        "id": "884e70e7-9659-4264-8ce6-8dc248be50eb",
        "jsonrpc": "2.0",
        "method": "offers.OfferSubmit",
        "params": {"offerID": offerId}
      },
    );

    try {
      return response.data['result'];
    } catch (e) {
      if (response.data['error']['code'] == -32603) {
        throw ('Необходимо войти в учетную запись');
      }
      throw (response.data['error']['message']);
    }
  }
}
