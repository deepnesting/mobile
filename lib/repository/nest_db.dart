import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:sqfentity/sqfentity.dart';
import 'package:sqfentity_gen/sqfentity_gen.dart';

part 'nest_db.g.dart';

const tableOffer = SqfEntityTable(
    tableName: 'offer',
    primaryKeyName: 'id',
    primaryKeyType: PrimaryKeyType.integer_auto_incremental,
    useSoftDeleting: false,
    modelName: null,
    fields: [
      SqfEntityField('webId', DbType.integer),
      SqfEntityField('type', DbType.integer),
      SqfEntityField('buildType', DbType.integer),
      SqfEntityField('title', DbType.text),
      SqfEntityField('publishedAt', DbType.datetime),
      SqfEntityField('price', DbType.real, defaultValue: 0),
      SqfEntityField('views', DbType.integer),
      SqfEntityField('isFavorite', DbType.bool),
      SqfEntityField('lat', DbType.real),
      SqfEntityField('lon', DbType.real),
      SqfEntityField('status', DbType.text),
      SqfEntityField('userWebId', DbType.integer),
      SqfEntityField('firstName', DbType.text),
      SqfEntityField('lastName', DbType.text),
      SqfEntityField('avatarUrl', DbType.text),
    ]);

const tableOfferImage = SqfEntityTable(
    tableName: 'offerImage',
    primaryKeyName: 'id',
    primaryKeyType: PrimaryKeyType.integer_auto_incremental,
    useSoftDeleting: false,
    modelName: null,
    fields: [
      SqfEntityField('imageUrl', DbType.text),
      SqfEntityFieldRelationship(
          parentTable: tableOffer,
          deleteRule: DeleteRule.CASCADE,
          defaultValue: '0'),
    ]);

const tableCheckedOffer = SqfEntityTable(
    tableName: 'checkedOffer',
    primaryKeyName: 'id',
    primaryKeyType: PrimaryKeyType.integer_auto_incremental,
    useSoftDeleting: false,
    modelName: null,
    fields: [
      SqfEntityField('offerId', DbType.integer),
    ]);

const tableFavoriteOffer = SqfEntityTable(
    tableName: 'favoriteOffer',
    primaryKeyName: 'id',
    primaryKeyType: PrimaryKeyType.integer_auto_incremental,
    useSoftDeleting: false,
    modelName: null,
    fields: [
      SqfEntityField('offerId', DbType.integer),
    ]);

@SqfEntityBuilder(nestDbModel)
const nestDbModel = SqfEntityModel(
  modelName: 'NestDbModel',
  databaseName: 'nest.db',
  databaseTables: [
    tableOffer,
    tableOfferImage,
    tableCheckedOffer,
    tableFavoriteOffer
  ],
  bundledDatabasePath: null,
);
