import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:flutter/foundation.dart';
import './offer_comment.dart';

class OfferCommentBloc extends Bloc<OfferCommentEvent, OfferCommentState> {
  final NestRepository nestRepository;

  OfferCommentBloc({@required this.nestRepository});

  @override
  OfferCommentState get initialState => HideOfferCommentState();

  @override
  Stream<OfferCommentState> mapEventToState(
    OfferCommentEvent event,
  ) async* {
    if (event is ShowOfferCommentEvent) {
      yield* _mapShowOfferCommentEventToState(event);
    } else if (event is HideOfferCommentEvent) {
      yield* _mapHideOfferCommentEventToState();
    } else if (event is SendOfferCommentEvent) {
      yield* _mapSendOfferCommentEventToState(event);
    }
  }

  Stream<OfferCommentState> _mapShowOfferCommentEventToState(
      ShowOfferCommentEvent event) async* {
    try {
      List<OfferCommentsModel> commentsList =
          await nestRepository.fetchOfferComments(offerId: event.offerId);
      yield ShowOfferCommentState(commentsList: commentsList);
    } catch (e) {
      yield ErrorOfferCommentState(error: e.toString());
    }
  }

  Stream<OfferCommentState> _mapHideOfferCommentEventToState() async* {
    yield HideOfferCommentState();
  }

  Stream<OfferCommentState> _mapSendOfferCommentEventToState(
      SendOfferCommentEvent event) async* {
    await nestRepository.sendComment(
        comment: event.comment, offerId: event.offerId);
    yield HideOfferCommentState();
    List<OfferCommentsModel> commentsList =
        await nestRepository.fetchOfferComments(offerId: event.offerId);
    yield ShowOfferCommentState(commentsList: commentsList);
  }
}
