import 'package:deepnesting/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class OfferCommentState extends Equatable {
  const OfferCommentState();
}

class HideOfferCommentState extends OfferCommentState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'InitialOfferCommentState';
}

class ShowOfferCommentState extends OfferCommentState {
  final List<OfferCommentsModel> commentsList;

  ShowOfferCommentState({@required this.commentsList});

  @override
  List<Object> get props => [];

  @override
  String toString() =>
      'ShowOfferCommentState {comments number: ${commentsList.length}';
}

class ErrorOfferCommentState extends OfferCommentState {
  final String error;

  ErrorOfferCommentState({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ErrorOfferCommentState';
}
