import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class OfferCommentEvent extends Equatable {
  const OfferCommentEvent();
}

class ShowOfferCommentEvent extends OfferCommentEvent {
  final int offerId;

  ShowOfferCommentEvent({@required this.offerId});

  @override
  List<Object> get props => [offerId];

  @override
  String toString() => 'ShowOfferCommentEvent {offerId: $offerId}';
}

class HideOfferCommentEvent extends OfferCommentEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'HideOfferCommentEvent';
}

class SendOfferCommentEvent extends OfferCommentEvent {
  final int offerId;
  final String comment;

  SendOfferCommentEvent({@required this.comment, @required this.offerId});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'SendOfferCommentEvent {comment $comment, offerId: $offerId}';
}
