import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';

import './offers.dart';

class OffersBloc extends Bloc<OffersEvent, OffersState> {
  final NestRepository nestRepository;
  List<int> checkedOffers = List();
  List<int> favoriteOffers = List();
  List<OfferModel> offers = List();
  List<PointModel> points = List();

  OffersBloc({@required this.nestRepository});

  @override
  OffersState get initialState => NotLoadedOffersState();

  @override
  Stream<OffersState> mapEventToState(
    OffersEvent event,
  ) async* {
    if (event is ClearOffersEvent) {
      yield* _mapClearOffersEventToState();
    } else if (event is LoadOffersEvent) {
      yield* _mapLoadOffersEventToState(event);
    } else if (event is GetCheckedOffersListEvent) {
      yield* _mapGetCheckedOffersListEventToState();
    } else if (event is AddOfferToCheckedOffersListEvent) {
      yield* _mapAddOfferToCheckedOffersListEventToState(event);
    } else if (event is AddOfferToFavoritesOffersListEvent) {
      yield* _mapAddOfferToFavoritesOffersListEventToState(event);
    } else if (event is GetFavoritesOffersListEvent) {
      yield* _mapGetFavoritesOffersListEventToState();
    } else if (event is RemoveOfferFromFavoritesOffersListEvent) {
      yield* _mapRemoveOfferFromFavoritesOffersListEventToState(event);
    }
  }

  Stream<OffersState> _mapGetCheckedOffersListEventToState() async* {
    checkedOffers = await nestRepository.getCheckedOffersFromDB();
  }

  /// Add checked offer to checked offers list
  Stream<OffersState> _mapAddOfferToCheckedOffersListEventToState(
      AddOfferToCheckedOffersListEvent event) async* {
    checkedOffers.add(event.offerId);
    nestRepository.saveCheckedOffersList(checkedOffers: checkedOffers);
  }

  Stream<OffersState> _mapGetFavoritesOffersListEventToState() async* {
    favoriteOffers = await nestRepository.getFavoriteOffersFromDB();
  }

  Stream<OffersState> _mapAddOfferToFavoritesOffersListEventToState(
      AddOfferToFavoritesOffersListEvent event) async* {
    favoriteOffers.add(event.offerId);
    nestRepository.saveFavoriteOffersList(favoriteOffers: favoriteOffers);
  }

  Stream<OffersState> _mapRemoveOfferFromFavoritesOffersListEventToState(
      RemoveOfferFromFavoritesOffersListEvent event) async* {
    favoriteOffers.remove(event.offerId);
    nestRepository.saveFavoriteOffersList(favoriteOffers: favoriteOffers);
  }

  Stream<OffersState> _mapClearOffersEventToState() async* {
    yield NotLoadedOffersState();
  }

  Stream<OffersState> _mapLoadOffersEventToState(LoadOffersEvent event) async* {
    /// if we provide only zoom value, we don't need to change any other variables
    if (event.onlyZoom == null || event.onlyZoom == false) {
      SharedPreferencesUtils.setIntVariableToShared(
        variable: event.priceFrom,
        title: 'PriceFrom',
      );
      SharedPreferencesUtils.setIntVariableToShared(
        variable: event.priceTo,
        title: 'PriceTo',
      );
      SharedPreferencesUtils.setIntVariableToShared(
        variable: event.searchType,
        title: 'SearchType',
      );
      SharedPreferencesUtils.setIntVariableToShared(
        variable: event.buildType,
        title: 'BuildType',
      );
    }
    SharedPreferencesUtils.setDoubleVariableToShared(
      variable: event.mapZoom,
      title: 'MapZoom',
    );

    int priceFrom = await SharedPreferencesUtils.getIntVariableFromShared(
      title: 'PriceFrom',
    );
    int priceTo = await SharedPreferencesUtils.getIntVariableFromShared(
      title: 'PriceTo',
    );
    int searchType = await SharedPreferencesUtils.getIntVariableFromShared(
      title: 'SearchType',
    );
    int buildType = await SharedPreferencesUtils.getIntVariableFromShared(
      title: 'BuildType',
    );
    double mapZoom = await SharedPreferencesUtils.getDoubleVariableFromShared(
      title: 'MapZoom',
    );
    if (mapZoom == null) {
      mapZoom = 12.0;
      SharedPreferencesUtils.setDoubleVariableToShared(
        variable: 12.0,
        title: 'MapZoom',
      );
    }

    List<BitmapDescriptor> markerImages = List();

    try {
      var result = await nestRepository.fetchOffers(
        cancelToken: event.cancelToken,
        offersIds: event.offersIds,
        priceFrom: priceFrom,
        priceTo: priceTo,
        searchType: searchType,
        buildType: buildType,
        mapZoom: mapZoom,
      );

      offers = result[0];
      points = result[1];

      for (int i = 0; i < points.length; i++) {
        bool ifCluster = points[i].type == 'cluster';
        int priceOrNumber = ifCluster ? points[i].count : offers[i].price;
        bool ifChecked = checkedOffers.contains(offers[i].id);
        var bitImg = await WidgetUtils.bitmapDescriptor(
            priceOrNumber: priceOrNumber,
            isCluster: ifCluster,
            ifCheckedOffer: ifChecked);
        //if there are no price we would show user avatar
        if (offers[i].price == 0 && points[i].type != 'cluster') {
          bitImg = await WidgetUtils.imageBitmapDescriptor(
              imageUrl: offers[i].user.avatarUrl, targetWidth: 100);
        }
        markerImages.add(bitImg);
      }

      yield LoadedOffersState(
        offers: offers,
        points: points,
        priceFrom: priceFrom,
        priceTo: priceTo,
        searchType: searchType,
        buildType: buildType,
        mapZoom: mapZoom,
        markerImages: markerImages,
        checkedOffers: checkedOffers,
        favoriteOffers: favoriteOffers,
      );
    } catch (e) {
      print(e);
      yield NotLoadedOffersState();
    }
  }
}
