import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:deepnesting/models/models.dart';

abstract class OffersState extends Equatable {
  const OffersState();
}

class NotLoadedOffersState extends OffersState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'NotLoadedOffersState';
}

class LoadedOffersState extends OffersState {
  final List<OfferModel> offers;
  final List<PointModel> points;
  final int priceFrom;
  final int priceTo;
  final int searchType;
  final int buildType;
  final double mapZoom;
  final List<BitmapDescriptor> markerImages;
  final List<int> checkedOffers;
  final List<int> favoriteOffers;

  LoadedOffersState({
    @required this.offers,
    this.points,
    this.priceFrom,
    this.priceTo,
    this.searchType,
    this.buildType,
    this.mapZoom,
    this.markerImages,
    this.checkedOffers,
    this.favoriteOffers,
  });

  @override
  List<Object> get props =>
      [offers, points, priceFrom, priceTo, searchType, buildType];

  @override
  String toString() => 'LoadedOffersState';
}
