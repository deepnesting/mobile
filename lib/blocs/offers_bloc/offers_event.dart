import 'package:equatable/equatable.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

abstract class OffersEvent extends Equatable {
  const OffersEvent();
}

class LoadOffersEvent extends OffersEvent {
  final List<int> offersIds;
  final int priceFrom;
  final int priceTo;
  final int searchType;
  final int buildType;
  final double mapZoom;
  final bool onlyZoom;
  final CancelToken cancelToken;

  LoadOffersEvent({
    this.offersIds,
    this.priceFrom,
    this.priceTo,
    this.searchType,
    this.buildType,
    this.mapZoom,
    this.onlyZoom,
    this.cancelToken,
  });

  @override
  List<Object> get props => [
        priceFrom,
        priceTo,
        searchType,
        buildType,
        mapZoom,
        onlyZoom,
        cancelToken
      ];

  @override
  String toString() =>
      'LoadOffersEvent {priceFrom: $priceFrom, priceTo: $priceTo, searchType: $searchType, buildType: $buildType, mapZoom: $mapZoom}';
}

class ClearOffersEvent extends OffersEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ClearOffersEvent';
}

class GetCheckedOffersListEvent extends OffersEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'GetCheckedOffersListEvent';
}

class AddOfferToCheckedOffersListEvent extends OffersEvent {
  final int offerId;

  AddOfferToCheckedOffersListEvent({@required this.offerId});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'AddOfferToCheckedOffersListEvent {offerId: $offerId}';
}

class GetFavoritesOffersListEvent extends OffersEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'GetFavoritesOffersListEvent';
}

class AddOfferToFavoritesOffersListEvent extends OffersEvent {
  final int offerId;

  AddOfferToFavoritesOffersListEvent({@required this.offerId});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'AddOfferToFavoritesOffersListEvent {offerId: $offerId}';
}

class RemoveOfferFromFavoritesOffersListEvent extends OffersEvent {
  final int offerId;

  RemoveOfferFromFavoritesOffersListEvent({@required this.offerId});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'DeleteOfferFromFavoritesOffersListEvent {offerId: $offerId}';
}