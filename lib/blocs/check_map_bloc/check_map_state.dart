import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class CheckMapState extends Equatable {
  const CheckMapState();
}

class InitialCheckMapState extends CheckMapState {
  @override
  List<Object> get props => [];
}

class SetCheckMapState extends CheckMapState {
  final bool ifMapDisabled;

  SetCheckMapState({@required this.ifMapDisabled});

  @override
  List<Object> get props => [ifMapDisabled];

  @override
  String toString() => 'SetCheckMapState {ifMapDisabled: $ifMapDisabled}';
}

class ErrorCheckMapState extends CheckMapState {
  final String error;

  ErrorCheckMapState({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ErrorCheckMapState {error: $error}';
}