import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:flutter/foundation.dart';
import './check_map.dart';

class CheckMapBloc extends Bloc<CheckMapEvent, CheckMapState> {
  final NestRepository nestRepository;

  CheckMapBloc({@required this.nestRepository});

  @override
  CheckMapState get initialState => InitialCheckMapState();

  @override
  Stream<CheckMapState> mapEventToState(
    CheckMapEvent event,
  ) async* {
    if (event is SendCheckMapEvent) {
      yield* _mapSendCheckMapEventToState();
    } else if (event is ClearCheckMapEvent) {
      yield* _mapClearCheckMapEventToState();
    }
  }

  Stream<CheckMapState> _mapSendCheckMapEventToState() async* {
    try {
      bool ifMapDisabled = await nestRepository.ifMapDisabled();
      yield SetCheckMapState(ifMapDisabled: ifMapDisabled);
    } catch (e) {
      yield ErrorCheckMapState(error: e.toString());
    }
  }

  Stream<CheckMapState> _mapClearCheckMapEventToState() async* {
    yield InitialCheckMapState();
  }
}
