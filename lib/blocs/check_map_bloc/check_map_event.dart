import 'package:equatable/equatable.dart';

abstract class CheckMapEvent extends Equatable {
  const CheckMapEvent();
}

class SendCheckMapEvent extends CheckMapEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'SendCheckMapEvent';
}

class ClearCheckMapEvent extends CheckMapEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ClearCheckMapEvent';
}