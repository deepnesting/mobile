import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:meta/meta.dart';
import './moderation.dart';

class ModerationBloc extends Bloc<ModerationEvent, ModerationState> {
  final NestRepository nestRepository;
  final LoginBloc loginBloc;
  StreamSubscription loginSubscription;
  bool ifAdmin = false;
  int userId;

  ModerationBloc({@required this.nestRepository, @required this.loginBloc}) {
    loginSubscription = loginBloc.state.listen((state) {
      if (state is LogInState) {
        ifAdmin = state.user.isModerator;
        userId = state.user.userId;
      }
    });
  }

  @override
  void dispose() {
    loginSubscription.cancel();
    super.dispose();
  }

  @override
  ModerationState get initialState => InitialModerationState();

  @override
  Stream<ModerationState> mapEventToState(
    ModerationEvent event,
  ) async* {
    if (event is SendToModerationEvent) {
      yield* _mapSendToModerationEvent(event);
    } else if (event is WithdrawModerationEvent) {
      yield* _mapWithdrawModerationEvent(event);
    } else if (event is PublishModerationEvent) {
      yield* _mapPublishModerationEventToState(event);
    }
  }

  Stream<ModerationState> _mapSendToModerationEvent(SendToModerationEvent event) async* {
    try {
      final result = await nestRepository
          .submitForModeration(offerId: event.offerId);
      if (result == null) {
        yield SuccessfullySubmittedModerationState();
      }
    } catch (e) {
      yield ErrorModerationState(error: e.toString());
    }
  }

  Stream<ModerationState> _mapWithdrawModerationEvent(WithdrawModerationEvent event) async* {
    try {
      final result = await nestRepository
          .unPublishOffer(offerId: event.offerId);
      if (result == null) {
        yield SuccessfullyWithdrawModerationState();
      }
    } catch (e) {
      yield ErrorModerationState(error: e.toString());
    }
  }

  Stream<ModerationState> _mapPublishModerationEventToState(PublishModerationEvent event) async* {
    try {
      final result = await nestRepository
          .submitOffer(offerId: event.offerId);
      if (result == null) {
        yield PublishModerationState();
      }
    } catch (e) {
      yield ErrorModerationState(error: e.toString());
    }
  }
}
