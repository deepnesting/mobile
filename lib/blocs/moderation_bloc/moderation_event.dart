import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class ModerationEvent extends Equatable {
  const ModerationEvent();
}

class SendToModerationEvent extends ModerationEvent {
  final int offerId;

  SendToModerationEvent({@required this.offerId});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'SendToModerationEvent {offerId: $offerId}';
}

class WithdrawModerationEvent extends ModerationEvent {
  final int offerId;

  WithdrawModerationEvent({@required this.offerId});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'SendToModerationEvent {offerId: $offerId}';
}

class PublishModerationEvent extends ModerationEvent {
  final int offerId;

  PublishModerationEvent({@required this.offerId});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'PublishModerationEvent {offerId: $offerId}';
}