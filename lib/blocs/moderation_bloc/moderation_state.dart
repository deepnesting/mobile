import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class ModerationState extends Equatable {
  const ModerationState();
}

class InitialModerationState extends ModerationState {
  @override
  List<Object> get props => [];
}

class SuccessfullySubmittedModerationState extends ModerationState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'SuccessfullySubmittedModerationState';
}

class SuccessfullyWithdrawModerationState extends ModerationState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'SuccessfullyWithdrawModerationState';
}

class PublishModerationState extends ModerationState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'PublishModerationState';
}

class ErrorModerationState extends ModerationState {
  final String error;

  ErrorModerationState({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ErrorModerationState {error: $error}';
}