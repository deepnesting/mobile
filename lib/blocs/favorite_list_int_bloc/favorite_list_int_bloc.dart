import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:meta/meta.dart';
import './favorite_list_int.dart';

class FavoriteListIntBloc
    extends Bloc<FavoriteListIntEvent, FavoriteListIntState> {
  final NestRepository nestRepository;
  List<int> favoriteOffersIds;

  FavoriteListIntBloc({@required this.nestRepository});

  @override
  FavoriteListIntState get initialState => NoFavoriteListIntState();

  @override
  Stream<FavoriteListIntState> mapEventToState(
    FavoriteListIntEvent event,
  ) async* {
    if (event is GetFavoriteListIntEvent) {
      yield* _mapGetFavoriteListIntEventToState();
    } else if (event is ClearFavoriteListIntEvent) {
      yield* _mapClearFavoriteListIntEventToState();
    } else if (event is AddToFavoriteListIntEvent) {
      yield* _mapAddToFavoriteListIntEventToState(event);
    } else if (event is RemoveFromFavoriteListIntEvent) {
      yield* _mapRemoveFromFavoriteListIntEventToState(event);
    }
  }

  Stream<FavoriteListIntState> _mapGetFavoriteListIntEventToState() async* {
    try {
      favoriteOffersIds = await nestRepository.getFavoriteOffersFromDB();
      yield NoFavoriteListIntState();
      yield GetFavoriteListIntState(favoriteList: favoriteOffersIds);
    } catch (e) {
      yield ErrorFavoriteListIntState(error: e.toString());
    }
  }

  Stream<FavoriteListIntState> _mapClearFavoriteListIntEventToState() async* {
    yield NoFavoriteListIntState();
  }

  Stream<FavoriteListIntState> _mapAddToFavoriteListIntEventToState(
      AddToFavoriteListIntEvent event) async* {
    try {
      favoriteOffersIds.add(event.offerId);
      await nestRepository.saveFavoriteOffersList(favoriteOffers: favoriteOffersIds);
      yield NoFavoriteListIntState();
      yield GetFavoriteListIntState(favoriteList: favoriteOffersIds);
    } catch (e) {
      yield ErrorFavoriteListIntState(error: e.toString());
    }
  }

  Stream<FavoriteListIntState> _mapRemoveFromFavoriteListIntEventToState(
      RemoveFromFavoriteListIntEvent event) async* {
    try {
      favoriteOffersIds.remove(event.offerId);
      await nestRepository.saveFavoriteOffersList(favoriteOffers: favoriteOffersIds);
      yield NoFavoriteListIntState();
      yield GetFavoriteListIntState(favoriteList: favoriteOffersIds);
    } catch (e) {
      yield ErrorFavoriteListIntState(error: e.toString());
    }
  }
}
