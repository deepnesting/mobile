import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class FavoriteListIntState extends Equatable {
  const FavoriteListIntState();
}

class NoFavoriteListIntState extends FavoriteListIntState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'NoFavoriteListIntState';
}

class GetFavoriteListIntState extends FavoriteListIntState {
  final List<int> favoriteList;

  GetFavoriteListIntState({@required this.favoriteList});

  @override
  List<Object> get props => [favoriteList];

  @override
  String toString() => 'NoFavoriteListIntState {favoriteList: $favoriteList}';
}

class ErrorFavoriteListIntState extends FavoriteListIntState {
  final String error;

  ErrorFavoriteListIntState({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ErrorFavoriteListIntState {error: $error}';
}
