import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class FavoriteListIntEvent extends Equatable {
  const FavoriteListIntEvent();
}

class GetFavoriteListIntEvent extends FavoriteListIntEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'GetFavoriteListIntEvent';
}

class ClearFavoriteListIntEvent extends FavoriteListIntEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ClearFavoriteListIntEvent';
}

class AddToFavoriteListIntEvent extends FavoriteListIntEvent {
  final int offerId;

  AddToFavoriteListIntEvent({@required this.offerId});

  @override
  List<Object> get props => [offerId];

  @override
  String toString() => 'AddToFavoriteListIntEvent {offerId: $offerId}';
}

class RemoveFromFavoriteListIntEvent extends FavoriteListIntEvent {
  final int offerId;

  RemoveFromFavoriteListIntEvent({@required this.offerId});

  @override
  List<Object> get props => [offerId];

  @override
  String toString() => 'RemoveFromFavoriteListIntEvent {offerId: $offerId}';
}