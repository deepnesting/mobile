import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:deepnesting/models/models.dart';

abstract class MainPageOffersState extends Equatable {
  const MainPageOffersState();
}

class MainPageNoOffersState extends MainPageOffersState {
  @override
  List<Object> get props => [];
}

class MainPageLoadOffersState extends MainPageOffersState {
  final List<OfferModel> offers;
  final List<PointModel> points;
  final List<int> favoriteOffers;
  final List<int> checkedOffers;

  MainPageLoadOffersState(
      {@required this.offers,
      this.points,
      this.favoriteOffers,
      this.checkedOffers});

  @override
  List<Object> get props => [offers, points, checkedOffers, favoriteOffers];

  @override
  String toString() => 'LoadedOffersState';
}
