import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class MainPageOffersEvent extends Equatable {
  const MainPageOffersEvent();
}

class LoadSpecificMainPageOffersEvent extends MainPageOffersEvent {
  final List<int> offersIds;

  LoadSpecificMainPageOffersEvent({@required this.offersIds});

  @override
  List<Object> get props => [offersIds];

  @override
  String toString() =>
      'LoadSpecificMainPageOffersEvent {offersIds: ${offersIds.toString()}}';
}

class ClearMainPageOffersEvent extends MainPageOffersEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ClearMainPageOffersEvent';
}
