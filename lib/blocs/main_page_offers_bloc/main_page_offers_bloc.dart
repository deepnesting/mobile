import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:meta/meta.dart';
import './main_page_offers.dart';

class MainPageOffersBloc
    extends Bloc<MainPageOffersEvent, MainPageOffersState> {
  final NestRepository nestRepository;
  List<int> checkedOffers = List();
  List<int> favoriteOffers = List();

  MainPageOffersBloc({@required this.nestRepository});

  @override
  MainPageOffersState get initialState => MainPageNoOffersState();

  @override
  Stream<MainPageOffersState> mapEventToState(
    MainPageOffersEvent event,
  ) async* {
    if (event is ClearMainPageOffersEvent) {
      yield* _mapClearMainPageOffersEventToState();
    } else if (event is LoadSpecificMainPageOffersEvent) {
      yield* _mapLoadSpecificMainPageOffersEventToState(event);
    }
  }

  Stream<MainPageOffersState> _mapClearMainPageOffersEventToState() async* {
    yield MainPageNoOffersState();
  }

  Stream<MainPageOffersState> _mapLoadSpecificMainPageOffersEventToState(
      LoadSpecificMainPageOffersEvent event) async* {
    try {
      checkedOffers = await nestRepository.getCheckedOffersFromDB();
      favoriteOffers = await nestRepository.getFavoriteOffersFromDB();
      var result = await nestRepository.fetchOffers(offersIds: event.offersIds);
      List<OfferModel> offers = result[0];
      List<PointModel> points = result[1];
      yield MainPageLoadOffersState(
          offers: offers, points: points, favoriteOffers: favoriteOffers);
    } catch (e) {
      print(e);
      yield MainPageNoOffersState();
    }
  }
}
