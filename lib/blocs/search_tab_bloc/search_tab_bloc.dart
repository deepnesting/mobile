import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:flutter/foundation.dart';
import './search_tab.dart';

class SearchTabBloc extends Bloc<SearchTabEvent, SearchTabState> {
  final NestRepository nestRepository;

  SearchTabBloc({@required this.nestRepository});

  @override
  SearchTabState get initialState => InitialSearchTabState();

  @override
  Stream<SearchTabState> mapEventToState(
    SearchTabEvent event,
  ) async* {
    if (event is GetLastSearchTabEvent) {
      yield* _mapGetLastSearchTabEventToState();
    } else if (event is SaveSearchTabEvent) {
      yield* _mapSaveSearchTabEventToState(event);
    } else if (event is ClearSearchTabEvent) {
      yield* _mapClearSearchTabEventToState();
    }
  }

  Stream<SearchTabState> _mapGetLastSearchTabEventToState() async* {
    try {
      SearchTabEnum searchTabEnum = await nestRepository.getLastSearchTab();
      yield GetLastSearchTabState(searchTabEnum: searchTabEnum);
    } catch(e) {
      yield ErrorSearchTabState(error: e.toString());
    }
  }

  Stream<SearchTabState> _mapSaveSearchTabEventToState(
      SaveSearchTabEvent event) async* {
    try {
      await nestRepository.setSearchTab(searchTabEnum: event.searchTabEnum);
    } catch(e) {
      yield ErrorSearchTabState(error: e.toString());
    }
  }

  Stream<SearchTabState> _mapClearSearchTabEventToState() async* {
    yield InitialSearchTabState();
  }
}
