import 'package:deepnesting/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class SearchTabState extends Equatable {
  const SearchTabState();
}

class InitialSearchTabState extends SearchTabState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'InitialSearchTabState';
}

class GetLastSearchTabState extends SearchTabState {
  final SearchTabEnum searchTabEnum;

  GetLastSearchTabState({@required this.searchTabEnum});

  @override
  List<Object> get props => [searchTabEnum];

  @override
  String toString() => 'GetLastSearchTabState {searchTabEnum: $searchTabEnum}';
}

class ErrorSearchTabState extends SearchTabState {
  final String error;

  ErrorSearchTabState({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ErrorSearchTabState {error: $error}';
}