import 'package:deepnesting/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class SearchTabEvent extends Equatable {
  const SearchTabEvent();
}

class GetLastSearchTabEvent extends SearchTabEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'GetLastSearchTabEvent';
}

class SaveSearchTabEvent extends SearchTabEvent {
  final SearchTabEnum searchTabEnum;

  SaveSearchTabEvent({@required this.searchTabEnum});

  @override
  List<Object> get props => [searchTabEnum];

  @override
  String toString() => 'SaveSearchTabEvent {searchTabEnum: $searchTabEnum}';
}

class ClearSearchTabEvent extends SearchTabEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ClearSearchTabEvent';
}