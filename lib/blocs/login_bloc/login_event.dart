import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class WaitingForLoginEvent extends LoginEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'WaitingForLoginEvent';
}

class CheckLoginEvent extends LoginEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'CheckLoginEvent';
}

class EnterLoginEvent extends LoginEvent {
  final String login;
  final String password;

  EnterLoginEvent({@required this.login, @required this.password});

  @override
  List<Object> get props => [login,password];

  @override
  String toString() => 'EnterLoginEvent {login: $login}';
}

class GoToRegisterWindowLoginEvent extends LoginEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'GoToRegisterWindowLoginEvent';
}

class RegisterLoginEvent extends LoginEvent {
  final String login;
  final String password;

  RegisterLoginEvent({@required this.login, @required this.password});

  @override
  List<Object> get props => [login,password];

  @override
  String toString() => 'CheckTokenToLoginEvent {login: $login}';
}

class LogOutEvent extends LoginEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'LogOutEvent';
}
