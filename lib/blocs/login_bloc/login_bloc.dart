import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:meta/meta.dart';
import '../blocks.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final NestRepository nestRepository;

  LoginBloc({@required this.nestRepository});

  @override
  LoginState get initialState => WaitingForLoginState();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is CheckLoginEvent) {
      yield* _mapCheckLoginEventToState();
    } else if (event is EnterLoginEvent) {
      yield* _mapEnterLoginEventToState(event);
    } else if (event is RegisterLoginEvent) {
      yield* _mapRegisterLoginEventToState(event);
    } else if (event is LogOutEvent) {
      yield* _mapLogOutEventToState();
    } else if (event is GoToRegisterWindowLoginEvent) {
      yield* _mapGoToRegisterWindowLoginEventToState();
    }
  }

  Stream<LoginState> _mapCheckLoginEventToState() async* {
    try {
      String login =
          await SharedPreferencesUtils.getVariableFromShared(title: 'Login');
      String password =
          await SharedPreferencesUtils.getVariableFromShared(title: 'Password');
      User user = await nestRepository.login(login: login, password: password);
      yield LogInState(user: user);
    } catch (e) {
      print(e);
      yield LogOutState();
    }
  }

  Stream<LoginState> _mapGoToRegisterWindowLoginEventToState() async* {
    yield ShowRegisterPageState();
  }

  Stream<LoginState> _mapEnterLoginEventToState(EnterLoginEvent event) async* {
    try {
      User user = await nestRepository.login(
          login: event.login, password: event.password);
      yield LogInState(user: user);
    } catch (e) {
      print(e);
      yield ErrorLoginState(error: e.toString());
    }
  }

  Stream<LoginState> _mapRegisterLoginEventToState(
      RegisterLoginEvent event) async* {
    try {
      User user = await nestRepository.register(
          login: event.login, password: event.password);
      yield LogInState(user: user);
    } catch (e) {
      print(e);
      yield ErrorLoginState(error: e.toString());
    }
  }

  Stream<LoginState> _mapLogOutEventToState() async* {
    try {
      await nestRepository.logout();
      yield LogOutState();
    } catch (e) {
      print(e);
      yield ErrorLoginState(error: e.toString());
    }
  }
}
