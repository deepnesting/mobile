import 'package:deepnesting/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class WaitingForLoginState extends LoginState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'WaitingForLoginState';
}

class LogOutState extends LoginState {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'LogOutState';
}

class ShowRegisterPageState extends LoginState {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ShowRegisterPageState';
}

class LogInState extends LoginState {
  final User user;

  LogInState({@required this.user});

  @override
  List<Object> get props => [user];

  @override
  String toString() =>
      'LogInState {user.lastName: ${user.lastName} user.firstName: ${user.firstName}}';
}

class ErrorLoginState extends LoginState {
  final String error;

  ErrorLoginState({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ErrorLoginState {error: $error}';
}
