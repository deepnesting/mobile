import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:flutter/foundation.dart';
import './favorite_offers.dart';

class FavoriteOffersBloc extends Bloc<FavoriteOffersEvent, FavoriteOffersState> {
  final NestRepository nestRepository;
  List<int> favoriteOffersIds;

  FavoriteOffersBloc({@required this.nestRepository});

  @override
  FavoriteOffersState get initialState => DoNotShowFavoriteOffersState();

  @override
  Stream<FavoriteOffersState> mapEventToState(
    FavoriteOffersEvent event,
  ) async* {
    if (event is FetchFavoriteOffersEvent) {
      yield* _mapFetchFavoriteOffersEventToState();
    } else if (event is ClearFavoriteOffersEvent) {
      yield* _mapClearFavoriteOffersEventToState();
    }
  }

  Stream<FavoriteOffersState> _mapFetchFavoriteOffersEventToState() async* {
    try {
      favoriteOffersIds = await nestRepository.getFavoriteOffersFromDB();
      List<OfferModel> result = List();
      if(favoriteOffersIds != null && favoriteOffersIds.length != 0) {
        var response = await nestRepository.fetchOffers(offersIds: favoriteOffersIds);
        result = response[0];
      }
      yield DoNotShowFavoriteOffersState();
      yield ListOfFavoriteOffersState(favoriteOffers: result);
    } catch(e) {
      yield ErrorFavoriteOffersState(error: e);
    }
  }

  Stream<FavoriteOffersState> _mapClearFavoriteOffersEventToState() async* {
    yield DoNotShowFavoriteOffersState();
  }
}
