import 'package:deepnesting/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class FavoriteOffersState extends Equatable {
  const FavoriteOffersState();
}

class DoNotShowFavoriteOffersState extends FavoriteOffersState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'FetchFavoriteOffersEvent';
}

class ListOfFavoriteOffersState extends FavoriteOffersState {
  final List<OfferModel> favoriteOffers;

  ListOfFavoriteOffersState({@required this.favoriteOffers});

  @override
  List<Object> get props => [favoriteOffers];

  @override
  String toString() => 'ListOfFavoriteOffersState';
}

class ErrorFavoriteOffersState extends FavoriteOffersState {
  final dynamic error;

  ErrorFavoriteOffersState({@required this.error});

  @override
  List<Object> get props => [error];
  @override
  String toString() => 'ErrorFavoriteOffersState {error: ${error.toString()}';
}
