import 'package:equatable/equatable.dart';

abstract class FavoriteOffersEvent extends Equatable {
  const FavoriteOffersEvent();
}

class FetchFavoriteOffersEvent extends FavoriteOffersEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'FetchFavoriteOffersEvent';
}

class ClearFavoriteOffersEvent extends FavoriteOffersEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ClearFavoriteOffersEvent';
}
