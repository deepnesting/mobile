import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/models/models.dart';
import './tab.dart';

class TabBloc extends Bloc<TabEvent, NestTabEnum> {
  @override
  NestTabEnum get initialState => NestTabEnum.search;

  @override
  Stream<NestTabEnum> mapEventToState(
    TabEvent event,
  ) async* {
    if(event is UpdateTabEvent) {
      yield event.tab;
    }
  }
}
