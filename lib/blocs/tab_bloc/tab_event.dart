import 'package:deepnesting/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class TabEvent extends Equatable {
  const TabEvent();
}

class UpdateTabEvent extends TabEvent {
  final NestTabEnum tab;

  UpdateTabEvent({@required this.tab});

  @override
  List<Object> get props => [tab];

  @override
  String toString() => 'UpdateTabEvent {tab: $tab}';
}