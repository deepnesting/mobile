import 'package:deepnesting/models/offer_contacts_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class OfferContactsState extends Equatable {
  const OfferContactsState();
}

class NoOfferContactsState extends OfferContactsState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'NoOfferContactsState';
}

class LoadedContactsState extends OfferContactsState {
  final List<OfferContactsModel> contactsList;

  LoadedContactsState({@required this.contactsList});

  @override
  List<Object> get props => [contactsList];

  @override
  String toString() =>
      'LoadedContactsState {first text in contactsList: ${contactsList[0].text}';
}
