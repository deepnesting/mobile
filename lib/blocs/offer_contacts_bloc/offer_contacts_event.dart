import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class OfferContactsEvent extends Equatable {
  const OfferContactsEvent();
}

class ClearContactsEvent extends OfferContactsEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ClearContactsEvent';
}

class LoadContactsEvent extends OfferContactsEvent {
  final int offerId;

  LoadContactsEvent({@required this.offerId});

  @override
  List<Object> get props => [offerId];

  @override
  String toString() => 'LoadContactsEvent {offerId: $offerId}';
}
