import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/models/offer_contacts_model.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:meta/meta.dart';
import './offer_contacts.dart';

class OfferContactsBloc extends Bloc<OfferContactsEvent, OfferContactsState> {
  final NestRepository nestRepository;

  OfferContactsBloc({@required this.nestRepository});

  @override
  OfferContactsState get initialState => NoOfferContactsState();

  @override
  Stream<OfferContactsState> mapEventToState(
    OfferContactsEvent event,
  ) async* {
    if (event is ClearContactsEvent) {
      yield* _mapClearContactsEventToState();
    } else if (event is LoadContactsEvent) {
      yield* _mapLoadContactsEventToState(event);
    }
  }

  Stream<OfferContactsState> _mapClearContactsEventToState() async* {
    yield NoOfferContactsState();
  }

  Stream<OfferContactsState> _mapLoadContactsEventToState(
      LoadContactsEvent event) async* {
    try {
      List<OfferContactsModel> contactsList =
          await nestRepository.fetchOfferContacts(offerId: event.offerId);
      yield LoadedContactsState(contactsList: contactsList);
    } catch (e) {
      print(e);
      yield NoOfferContactsState();
    }
  }
}
