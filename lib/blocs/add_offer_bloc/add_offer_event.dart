import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AddOfferEvent extends Equatable {
  const AddOfferEvent();
}

class DoNotAddOfferEvent extends AddOfferEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'DoNotAddOfferEvent';
}

class SendAddOfferEvent extends AddOfferEvent {
  final int searchType;
  final int buildType;
  final int roomCount;
  final String comment;
  final int price;
  final String address;
  final List<File> images;

  SendAddOfferEvent(
      {@required this.searchType,
      @required this.buildType,
      @required this.roomCount,
      @required this.comment,
      @required this.price,
      @required this.address,
      @required this.images});

  @override
  List<Object> get props =>
      [searchType, buildType, roomCount, comment, price, address];

  @override
  String toString() => 'SendAddOfferEvent';
}

class LoadingAddOfferEvent extends AddOfferEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'LoadingAddOfferEvent';
}