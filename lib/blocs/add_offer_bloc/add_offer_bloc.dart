import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:meta/meta.dart';
import './add_offer.dart';

class AddOfferBloc extends Bloc<AddOfferEvent, AddOfferState> {
  final NestRepository nestRepository;

  AddOfferBloc({@required this.nestRepository});

  @override
  AddOfferState get initialState => InitialAddOfferState();

  @override
  Stream<AddOfferState> mapEventToState(
    AddOfferEvent event,
  ) async* {
    if (event is DoNotAddOfferEvent) {
      yield* _mapDoNotAddOfferEventToState();
    } else if (event is SendAddOfferEvent) {
      yield* _mapSendAddOfferEventToState(event);
    } else if (event is LoadingAddOfferEvent) {
      yield* _mapLoadingAddOfferEventToState();
    }
  }

  Stream<AddOfferState> _mapLoadingAddOfferEventToState() async* {
    yield LoadingAddOfferState();
  }

  Stream<AddOfferState> _mapDoNotAddOfferEventToState() async* {
    yield InitialAddOfferState();
  }

  Stream<AddOfferState> _mapSendAddOfferEventToState(
      SendAddOfferEvent event) async* {
    List<ImageModel> images = List();
    try {
      if (event.images == null || event.images.length == 0) {
        yield ErrorOfAddOfferState(error: 'К объявлению необходимо добавить фотографии');
      } else {
        for (int i = 0; i < event.images.length; i++) {
          ImageModel imageUrl =
          await nestRepository.uploadPhoto(photo: event.images[i]);
          images.add(imageUrl);
        }

        int offerId = await nestRepository.createOffer(
          searchType: event.searchType,
          buildType: event.buildType,
          roomCount: event.roomCount,
          comment: event.comment,
          price: event.price,
          address: event.address,
          images: images,
        );

        OfferSingleModel offer = await nestRepository.fetchSingleOffer(offerId: offerId);
        yield ResultOfAddOfferState(offer: offer);
      }
    } catch (e) {
      print('error send add offer $e');
      yield ErrorOfAddOfferState(error: e);
    }
  }
}
