import 'package:deepnesting/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class AddOfferState extends Equatable {
  const AddOfferState();
}

class InitialAddOfferState extends AddOfferState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'InitialAddOfferState';
}

class ResultOfAddOfferState extends AddOfferState {
  final OfferSingleModel offer;

  ResultOfAddOfferState({@required this.offer});

  @override
  List<Object> get props => [offer];

  @override
  String toString() => 'ResultOfAddOfferState {offerId: ${offer.id}';
}

class ErrorOfAddOfferState extends AddOfferState {
  final dynamic error;

  ErrorOfAddOfferState({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ErrorOfAddOfferState {error: $error}';
}

class LoadingAddOfferState extends AddOfferState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'LoadingAddOfferState';
}