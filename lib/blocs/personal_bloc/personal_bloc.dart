import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:meta/meta.dart';
import '../blocks.dart';
import './personal.dart';

class PersonalBloc extends Bloc<PersonalEvent, PersonalState> {
  final NestRepository nestRepository;
  final LoginBloc loginBloc;
  StreamSubscription loginBlocSubscription;

  PersonalBloc({@required this.loginBloc, @required this.nestRepository}) {
    loginBlocSubscription = loginBloc.state.listen((state) {
      if (state is LogInState) {
        dispatch(LoadingPersonalEvent());
        dispatch(ShowPersonalEvent());
      }
    });
  }

  @override
  void dispose() {
    loginBlocSubscription.cancel();
    super.dispose();
  }

  @override
  PersonalState get initialState => NoInfoPersonalState();

  @override
  Stream<PersonalState> mapEventToState(
    PersonalEvent event,
  ) async* {
    if (event is LoadingPersonalEvent) {
      yield* _mapLoadingPersonalEventToState();
    } else if (event is ShowPersonalEvent) {
      yield* _mapShowPersonalEventToState();
    } else if (event is UpdatePersonalEvent) {
      yield* _mapUpdatePersonalEventToState(event);
    }
  }

  Stream<PersonalState> _mapLoadingPersonalEventToState() async* {
    yield NoInfoPersonalState();
  }

  Stream<PersonalState> _mapShowPersonalEventToState() async* {
    try {
      UserMe userMe = await nestRepository.getUserInfo();
      yield InfoPersonalState(userMe: userMe);
    } catch (e) {
      ErrorPersonalState(error: e);
    }
  }

  Stream<PersonalState> _mapUpdatePersonalEventToState(
      UpdatePersonalEvent event) async* {
    try {
      String avatar = event.avatar;
      if (event.userPhoto != null) {
        ImageModel imageUrl =
        await nestRepository.uploadPhoto(photo: event.userPhoto);
        avatar = WidgetUtils.getFullSizeImageFromImageModel(image: imageUrl);
      }

      // update user credentials
      await nestRepository.updateUserInfo(
          firstName: event.firstName,
          lastName: event.lastName,
          avatar: avatar,
      );

      // update user profile data
      await nestRepository.updateUserProfileData(
        bDate: event.birthday.toString(),
        homeTown: event.hometown,
        info: event.userBio,
      );

      yield NoInfoPersonalState();
      UserMe userMe = await nestRepository.getUserInfo();
      yield InfoPersonalState(userMe: userMe);
    } catch (e) {
      print(e);
      ErrorPersonalState(error: e);
    }
  }
}
