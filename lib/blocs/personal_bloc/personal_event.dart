import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PersonalEvent extends Equatable {
  const PersonalEvent();
}

class LoadingPersonalEvent extends PersonalEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'LoadingPersonalEvent';
}

class ShowPersonalEvent extends PersonalEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ShowPersonalEvent';
}

class UpdatePersonalEvent extends PersonalEvent {
  final String firstName;
  final String lastName;
  final String avatar;
  final File userPhoto;
  final DateTime birthday;
  final String hometown;
  final String userBio;

  UpdatePersonalEvent({
    this.firstName,
    this.lastName,
    this.avatar,
    this.userPhoto,
    this.birthday,
    this.hometown,
    this.userBio,
  });

  @override
  List<Object> get props => [firstName, lastName, avatar, userPhoto];

  @override
  String toString() =>
      'UpdatePersonalEvent {firstName: $firstName, lastName: $lastName}';
}
