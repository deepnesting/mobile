import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:meta/meta.dart';
import './user_offers.dart';

class UserOffersBloc extends Bloc<UserOffersEvent, UserOffersState> {
  final NestRepository nestRepository;
  final LoginBloc loginBloc;
  StreamSubscription loginSubscription;

  UserOffersBloc({@required this.nestRepository, @required this.loginBloc}) {
    loginSubscription = loginBloc.state.listen((state) {
      if (state is LogInState) {
        dispatch(FetchUserOffersEvent(userId: state.user.userId));
      } else if (state is LogOutState) {
        dispatch(ClearUserOffersEvent());
      }
    });
  }

  @override
  UserOffersState get initialState => ClearUserOffersState();

  @override
  void dispose() {
    loginSubscription.cancel();
    super.dispose();
  }

  @override
  Stream<UserOffersState> mapEventToState(
    UserOffersEvent event,
  ) async* {
    if (event is ClearUserOffersEvent) {
      yield* _mapClearUserOffersEventToState();
    } else if (event is FetchUserOffersEvent) {
      yield* _mapFetchUserOffersEventToState(event);
    } else if (event is SubmitForModerationUserOffersEvent) {
      yield* _mapSubmitForModerationUserOffersEventToState(event);
    }
  }

  Stream<UserOffersState> _mapClearUserOffersEventToState() async* {
    yield ClearUserOffersState();
  }

  Stream<UserOffersState> _mapFetchUserOffersEventToState(
      FetchUserOffersEvent event) async* {
    try {
      final result = await nestRepository
          .fetchOffers(userId: event.userId, statuses: [10, 11, 12]);
      final List<OfferModel> userOffers = result[0];

      yield ListOfUserOffersState(userOffers: userOffers);
    } catch (e) {
      yield ErrorUserOffersState(error: e);
    }
  }

  Stream<UserOffersState> _mapSubmitForModerationUserOffersEventToState(
      SubmitForModerationUserOffersEvent event) async* {
    try {
      final result = await nestRepository
          .submitForModeration(offerId: event.offerId);
      if (result == null) {
        yield SuccessfullySubmittedUserOffersState();
      }
    } catch (e) {
      yield ErrorUserOffersState(error: e);
    }
  }
}
