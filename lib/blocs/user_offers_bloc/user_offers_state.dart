import 'package:deepnesting/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class UserOffersState extends Equatable {
  const UserOffersState();
}

class ClearUserOffersState extends UserOffersState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'InitialUserOffersState';
}

class ListOfUserOffersState extends UserOffersState {
  final List<OfferModel> userOffers;

  ListOfUserOffersState({@required this.userOffers});

  @override
  List<Object> get props => [userOffers];

  @override
  String toString() =>
      'ListOfUserOffersState';
}

class ErrorUserOffersState extends UserOffersState {
  final dynamic error;

  ErrorUserOffersState({@required this.error});

  @override
  List<Object> get props => [error];
  @override
  String toString() => 'ErrorUserOffersState {error: ${error.toString()}';
}

class SuccessfullySubmittedUserOffersState extends UserOffersState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'SubmittedUserOffersState';
}
