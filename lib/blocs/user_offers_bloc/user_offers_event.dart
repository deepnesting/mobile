import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class UserOffersEvent extends Equatable {
  const UserOffersEvent();
}

class FetchUserOffersEvent extends UserOffersEvent {
  final int userId;

  FetchUserOffersEvent({@required this.userId});

  @override
  List<Object> get props => [userId];

  @override
  String toString() => 'FetchUserOffersEvent {userId: $userId}';
}

class ClearUserOffersEvent extends UserOffersEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ClearUserOffersEvent';
}

class SubmitForModerationUserOffersEvent extends UserOffersEvent {
  final int offerId;

  SubmitForModerationUserOffersEvent({@required this.offerId});

  @override
  List<Object> get props => [offerId];

  @override
  String toString() => 'SubmitForModerationUserOffersEvent {offerId: $offerId}';
}