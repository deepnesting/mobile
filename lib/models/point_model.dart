import 'package:json_annotation/json_annotation.dart';

part 'point_model.g.dart';
@JsonSerializable()
class PointModel {
  @JsonKey(nullable:true)
  int id;
  @JsonKey(nullable:false)
  String type;
  @JsonKey(nullable:true)
  String geoId;
  @JsonKey(nullable:false)
  double lat;
  @JsonKey(nullable:false)
  double lon;
  @JsonKey(nullable:true)
  int count;
  @JsonKey(nullable:true)
  List<int> items;

  PointModel({
    this.id,
    this.type,
    this.geoId,
    this.lat,
    this.lon,
    this.count,
    this.items,
  });

  factory PointModel.fromJson(Map<String, dynamic> json) => _$PointModelFromJson(json);
  Map<String, dynamic> toJson() => _$PointModelToJson(this);

  static String get storageKey {
    return 'point_model';
  }
}