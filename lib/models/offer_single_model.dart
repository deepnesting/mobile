import 'package:deepnesting/models/models.dart';
import 'package:json_annotation/json_annotation.dart';

part 'offer_single_model.g.dart';

@JsonSerializable()
class OfferSingleModel {
  @JsonKey(name: 'offerId', nullable:false)
  int id;
  @JsonKey(nullable:false)
  int searchType;
  @JsonKey(nullable:false)
  int buildType;
  @JsonKey(nullable:false)
  int rentDuration;
  @JsonKey(nullable:false)
  int roomCount;
  @JsonKey(nullable:false)
  String title;
  @JsonKey(nullable:false)
  String publishedAt;
  @JsonKey(nullable:false)
  List<ImageModel> images;
  @JsonKey(nullable:false)
  String address;
  @JsonKey(nullable:false)
  int price;
  @JsonKey(nullable:false)
  String comment;
  @JsonKey(nullable:false)
  num lat;
  @JsonKey(nullable:false)
  num lon;
  @JsonKey(nullable:false)
  int status;
  @JsonKey(nullable:false)
  String publishedUntil;
  @JsonKey(nullable:false)
  int city;
  @JsonKey(nullable:false)
  UserMe user;


  OfferSingleModel({
  this.id,
  this.searchType,
  this.buildType,
  this.rentDuration,
  this.roomCount,
  this.title,
  this.publishedAt,
  this.images,
  this.address,
  this.price,
  this.comment,
  this.lat,
  this.lon,
  this.status,
  this.publishedUntil,
  this.city,
  this.user,
  });

  factory OfferSingleModel.fromJson(Map<String, dynamic> json) => _$OfferSingleModelFromJson(json);
  Map<String, dynamic> toJson() => _$OfferSingleModelToJson(this);

  static String get storageKey {
    return 'offer_single_model';
  }
}
