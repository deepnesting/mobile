// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer_comments_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OfferCommentsModel _$OfferCommentsModelFromJson(Map<String, dynamic> json) {
  return OfferCommentsModel(
    id: json['id'] as int,
    text: json['text'] as String,
    user: CommentUserModel.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OfferCommentsModelToJson(OfferCommentsModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'user': instance.user,
    };
