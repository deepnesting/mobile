// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer_user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OfferUserModel _$OfferUserModelFromJson(Map<String, dynamic> json) {
  return OfferUserModel(
    id: json['id'] as int,
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    avatarUrl: json['avatarUrl'] as String,
  );
}

Map<String, dynamic> _$OfferUserModelToJson(OfferUserModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'avatarUrl': instance.avatarUrl,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
    };
