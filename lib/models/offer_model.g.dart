// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OfferModel _$OfferModelFromJson(Map<String, dynamic> json) {
  return OfferModel(
    id: json['id'] as int,
    type: json['type'] as int,
    buildType: json['build_type'] as int,
    title: json['title'] as String,
    images: (json['images'] as List).map((e) => e as String).toList(),
    publishedAt: json['publishedAt'] as String,
    price: json['price'] as int,
    views: json['views'] as int,
    isFavorite: json['isFavorite'] as bool,
    user: OfferUserModel.fromJson(json['user'] as Map<String, dynamic>),
    lat: (json['lat'] as num).toDouble(),
    lon: (json['lon'] as num).toDouble(),
    status: json['status'] as String,
    statusId: json['statusId'] as int,
    comment: json['comment'] as String,
  );
}

Map<String, dynamic> _$OfferModelToJson(OfferModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'build_type': instance.buildType,
      'title': instance.title,
      'images': instance.images,
      'publishedAt': instance.publishedAt,
      'price': instance.price,
      'views': instance.views,
      'isFavorite': instance.isFavorite,
      'user': instance.user,
      'lat': instance.lat,
      'lon': instance.lon,
      'status': instance.status,
      'statusId': instance.statusId,
      'comment': instance.comment,
    };
