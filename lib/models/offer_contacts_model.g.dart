// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer_contacts_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OfferContactsModel _$OfferContactsModelFromJson(Map<String, dynamic> json) {
  return OfferContactsModel(
    type: json['type'] as String,
    text: json['text'] as String,
    value: json['value'] as String,
  );
}

Map<String, dynamic> _$OfferContactsModelToJson(OfferContactsModel instance) =>
    <String, dynamic>{
      'type': instance.type,
      'text': instance.text,
      'value': instance.value,
    };
