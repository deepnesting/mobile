import 'package:json_annotation/json_annotation.dart';

part 'comment_user_model.g.dart';

@JsonSerializable()
class CommentUserModel {
  @JsonKey(nullable:false)
  int id;
  @JsonKey(nullable:false)
  String name;
  @JsonKey(nullable:false)
  String avatarURL;

  CommentUserModel({
    this.id,
    this.name,
    this.avatarURL,
  });

  factory CommentUserModel.fromJson(Map<String, dynamic> json) => _$CommentUserModelFromJson(json);
  Map<String, dynamic> toJson() => _$CommentUserModelToJson(this);

  static String get storageKey {
    return 'comment_user_model';
  }
}