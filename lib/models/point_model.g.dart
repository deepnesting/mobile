// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'point_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PointModel _$PointModelFromJson(Map<String, dynamic> json) {
  return PointModel(
    id: json['id'] as int,
    type: json['type'] as String,
    geoId: json['geoId'] as String,
    lat: (json['lat'] as num).toDouble(),
    lon: (json['lon'] as num).toDouble(),
    count: json['count'] as int,
    items: (json['items'] as List)?.map((e) => e as int)?.toList(),
  );
}

Map<String, dynamic> _$PointModelToJson(PointModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'geoId': instance.geoId,
      'lat': instance.lat,
      'lon': instance.lon,
      'count': instance.count,
      'items': instance.items,
    };
