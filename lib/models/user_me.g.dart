// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_me.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserMe _$UserMeFromJson(Map<String, dynamic> json) {
  return UserMe(
    userId: json['userId'] as int,
    id: json['ID'] as int,
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    sex: json['Sex'] as String,
    avatarUrl: json['avatarUrl'] as String,
    status: json['Status'] as int,
    role: json['role'] as int,
    isVkUser: json['isVkUser'] as bool,
    vkId: json['vkId'] as int,
    email: json['email'] as String,
    birthdayDate: json['bdate'] as String,
    bio: json['bio'] as String,
    hometown: json['hometown'] as String,
    password: json['Password'] as String,
  );
}

Map<String, dynamic> _$UserMeToJson(UserMe instance) => <String, dynamic>{
      'userId': instance.userId,
      'ID': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'Sex': instance.sex,
      'avatarUrl': instance.avatarUrl,
      'Status': instance.status,
      'role': instance.role,
      'isVkUser': instance.isVkUser,
      'vkId': instance.vkId,
      'email': instance.email,
      'bdate': instance.birthdayDate,
      'bio': instance.bio,
      'hometown': instance.hometown,
      'Password': instance.password,
    };
