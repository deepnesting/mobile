import 'package:json_annotation/json_annotation.dart';

part 'offer_contacts_model.g.dart';

@JsonSerializable()
class OfferContactsModel {
  @JsonKey(nullable:false)
  String type;
  @JsonKey(nullable:false)
  String text;
  @JsonKey(nullable:false)
  String value;

  OfferContactsModel({
    this.type,
    this.text,
    this.value,
  });

  factory OfferContactsModel.fromJson(Map<String, dynamic> json) => _$OfferContactsModelFromJson(json);
  Map<String, dynamic> toJson() => _$OfferContactsModelToJson(this);

  static String get storageKey {
    return 'offer_contacts';
  }
}

