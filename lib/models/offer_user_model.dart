import 'package:json_annotation/json_annotation.dart';

part 'offer_user_model.g.dart';

@JsonSerializable()
class OfferUserModel {
  @JsonKey(nullable:false)
  int id;
  @JsonKey(nullable:false)
  String avatarUrl;
  @JsonKey(nullable:false)
  String firstName;
  @JsonKey(nullable:false)
  String lastName;

  OfferUserModel({
    this.id,
    this.firstName,
    this.lastName,
    this.avatarUrl,
  });

  factory OfferUserModel.fromJson(Map<String, dynamic> json) => _$OfferUserModelFromJson(json);
  Map<String, dynamic> toJson() => _$OfferUserModelToJson(this);

  static String get storageKey {
    return 'offer_user_model';
  }
}

