import 'package:deepnesting/models/models.dart';
import 'package:json_annotation/json_annotation.dart';

part 'offer_model.g.dart';

@JsonSerializable()
class OfferModel {
  @JsonKey(nullable:false)
  int id;
  @JsonKey(nullable:false)
  int type;
  @JsonKey(name: 'build_type',nullable:false)
  int buildType;
  @JsonKey(nullable:false)
  String title;
  @JsonKey(nullable:false)
  List<String> images;
  @JsonKey(nullable:false)
  String publishedAt;
  @JsonKey(nullable:false)
  int price;
  @JsonKey(nullable:false)
  int views;
  @JsonKey(nullable:false)
  bool isFavorite;
  @JsonKey(nullable:false)
  OfferUserModel user;
  @JsonKey(nullable:false)
  double lat;
  @JsonKey(nullable:false)
  double lon;
  @JsonKey(nullable:false)
  String status;
  @JsonKey(nullable: false)
  int statusId;
  @JsonKey(nullable:false)
  String comment;

  OfferModel({
    this.id,
    this.type,
    this.buildType,
    this.title,
    this.images,
    this.publishedAt,
    this.price,
    this.views,
    this.isFavorite,
    this.user,
    this.lat,
    this.lon,
    this.status,
    this.statusId,
    this.comment,
  });

  factory OfferModel.fromJson(Map<String, dynamic> json) => _$OfferModelFromJson(json);
  Map<String, dynamic> toJson() => _$OfferModelToJson(this);

  static String get storageKey {
    return 'offer_model';
  }
}
