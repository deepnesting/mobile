// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer_single_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OfferSingleModel _$OfferSingleModelFromJson(Map<String, dynamic> json) {
  return OfferSingleModel(
    id: json['offerId'] as int,
    searchType: json['searchType'] as int,
    buildType: json['buildType'] as int,
    rentDuration: json['rentDuration'] as int,
    roomCount: json['roomCount'] as int,
    title: json['title'] as String,
    publishedAt: json['publishedAt'] as String,
    images: (json['images'] as List)
        .map((e) => ImageModel.fromJson(e as Map<String, dynamic>))
        .toList(),
    address: json['address'] as String,
    price: json['price'] as int,
    comment: json['comment'] as String,
    lat: json['lat'] as num,
    lon: json['lon'] as num,
    status: json['status'] as int,
    publishedUntil: json['publishedUntil'] as String,
    city: json['city'] as int,
    user: UserMe.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OfferSingleModelToJson(OfferSingleModel instance) =>
    <String, dynamic>{
      'offerId': instance.id,
      'searchType': instance.searchType,
      'buildType': instance.buildType,
      'rentDuration': instance.rentDuration,
      'roomCount': instance.roomCount,
      'title': instance.title,
      'publishedAt': instance.publishedAt,
      'images': instance.images,
      'address': instance.address,
      'price': instance.price,
      'comment': instance.comment,
      'lat': instance.lat,
      'lon': instance.lon,
      'status': instance.status,
      'publishedUntil': instance.publishedUntil,
      'city': instance.city,
      'user': instance.user,
    };
