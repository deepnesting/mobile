// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment_user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentUserModel _$CommentUserModelFromJson(Map<String, dynamic> json) {
  return CommentUserModel(
    id: json['id'] as int,
    name: json['name'] as String,
    avatarURL: json['avatarURL'] as String,
  );
}

Map<String, dynamic> _$CommentUserModelToJson(CommentUserModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'avatarURL': instance.avatarURL,
    };
