import 'package:json_annotation/json_annotation.dart';

part 'parameters.g.dart';

@JsonSerializable()
class Parameters {
  @JsonKey(nullable: false)
  int promotedBy;
  @JsonKey(nullable: true)
  String attributes;
  @JsonKey(nullable: false)
  String email;
  @JsonKey(nullable: false)
  String bio;
  @JsonKey(name: 'bdate', nullable: false)
  String birthdayDate;
  @JsonKey(nullable: false)
  String hometown;

  Parameters({
    this.promotedBy,
    this.attributes,
    this.email,
    this.bio,
    this.birthdayDate,
    this.hometown,
  });

  factory Parameters.fromJson(Map<String, dynamic> json) =>
      _$ParametersFromJson(json);
  Map<String, dynamic> toJson() => _$ParametersToJson(this);

  static String get storageKey {
    return 'parameters';
  }
}
