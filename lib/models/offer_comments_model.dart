import 'package:deepnesting/models/models.dart';
import 'package:json_annotation/json_annotation.dart';

part 'offer_comments_model.g.dart';

@JsonSerializable()
class OfferCommentsModel {
  @JsonKey(nullable:false)
  int id;
  @JsonKey(nullable:false)
  String text;
  @JsonKey(nullable:false)
  CommentUserModel user;

  OfferCommentsModel({
    this.id,
    this.text,
    this.user,
  });

  factory OfferCommentsModel.fromJson(Map<String, dynamic> json) => _$OfferCommentsModelFromJson(json);
  Map<String, dynamic> toJson() => _$OfferCommentsModelToJson(this);

  static String get storageKey {
    return 'offer_comments_model';
  }
}

