import 'package:deepnesting/tabs/tabs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:async';

import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/widgets/widgets.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class HomeScreen extends StatefulWidget {
  static const double panelHeightOpen = 500.0;
  static const double panelHeightClosed = 70.0;
  final bool ifMapDisabled;

  HomeScreen({@required this.ifMapDisabled});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  final List<Tab> nestTabs = [
    Tab(
        child: Column(
      children: <Widget>[
        Expanded(
          child: Icon(
            FontAwesomeIcons.search,
            color: nestGrayIcons,
          ),
        ),
      ],
    )),
    Tab(
      child: Column(
        children: <Widget>[
          Expanded(
            child: Icon(
              FontAwesomeIcons.comment,
              color: nestGrayIcons,
            ),
          ),
        ],
      ),
    ),
    Tab(
      child: Column(
        children: <Widget>[
          Expanded(
              child: Container(
            width: 45.0,
            height: 45.0,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 0.0,
              color: nestCyan,
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
          )),
        ],
      ),
    ),
    Tab(
      child: Column(
        children: <Widget>[
          Expanded(
            child: Icon(
              FontAwesomeIcons.bell,
              color: nestGrayIcons,
            ),
          ),
        ],
      ),
    ),
    Tab(
      child: Column(
        children: <Widget>[
          Expanded(
            child: Icon(
              FontAwesomeIcons.user,
              color: nestGrayIcons,
            ),
          ),
        ],
      ),
    ),
  ];

  TabBloc _tabBloc;
  LoginBloc _loginBloc;
  OffersBloc _offersBloc;
  SearchTabBloc _searchTabBloc;
  FavoriteListIntBloc _favoriteListIntBloc;
  TabController _tabController;
  NestTabEnum initialTab = NestTabEnum.search;
  double _height = 0;
  double _tabStartHeight = 0;
  final double _initFabHeight = HomeScreen.panelHeightClosed + 20.0;
  double _fabHeight;
  ScrollPhysics physics;
  GlobalKey _keyMap = GlobalKey();
  List<OfferModel> _offers;
  List<PointModel> _points;
  List<BitmapDescriptor> _markerImages;
  double _initialZoom = 12.0;
  Timer timer;
  bool _isMap;
  double _appBarHeight = 75.0;
  bool _showMapButton;

  @override
  void initState() {
    super.initState();
    _tabBloc = BlocProvider.of<TabBloc>(context);
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _loginBloc.dispatch(CheckLoginEvent());
    _tabController = TabController(
        vsync: this,
        length: nestTabs.length,
        initialIndex: NestTabEnum.values.indexOf(initialTab));
    _tabBloc.dispatch(UpdateTabEvent(tab: initialTab));
    _offersBloc = BlocProvider.of<OffersBloc>(context);
    _offersBloc.dispatch(GetCheckedOffersListEvent());
    _offersBloc.dispatch(GetFavoritesOffersListEvent());
    _offersBloc.dispatch(LoadOffersEvent());
    _fabHeight = _initFabHeight;
    _offers = List();
    _points = List();
    _markerImages = List();
    _isMap = false;
    _height = _isMap ? _appBarHeight : 0.0;
    _searchTabBloc = BlocProvider.of<SearchTabBloc>(context);
    _searchTabBloc.dispatch(GetLastSearchTabEvent());
    _isMap = widget.ifMapDisabled;
    _showMapButton = !widget.ifMapDisabled;
    changeAppBarHeightAndPhysics(condition: _showMapButton);
    _favoriteListIntBloc = BlocProvider.of<FavoriteListIntBloc>(context);
    _favoriteListIntBloc.dispatch(GetFavoriteListIntEvent());
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  switchBetweenMapAndHome() {
    setState(() {
      changeAppBarHeightAndPhysics(condition: _isMap);
      _isMap = !_isMap;
    });
  }

  changeAppBarHeightAndPhysics({@required bool condition}) {
    if (condition) {
      _height = 0;
      physics = NeverScrollableScrollPhysics();
    } else {
      _height = _appBarHeight;
      physics = ClampingScrollPhysics();
    }
  }

  getCachedOffersAndPoints() async {}

  onPanelSlide({@required double position}) {
    setState(() {
      _fabHeight = position *
              (HomeScreen.panelHeightOpen - HomeScreen.panelHeightClosed) +
          _initFabHeight;
    });
  }

  onButtonPressed() {
    setState(() {
      _fabHeight = _initFabHeight;
    });
  }

  Widget errorScreen({@required String errorMessage}) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                'Произошла ошибка: $errorMessage',
                textAlign: TextAlign.center,
                maxLines: 3,
                style: TextStyle(fontSize: SizeConfig.safeBlockAverage * 3),
              ),
              RaisedButton(
                child: Text(
                  'Попробовать снова',
                  style: TextStyle(
                    fontSize: SizeConfig.safeBlockAverage * 3,
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
                  _searchTabBloc.dispatch(GetLastSearchTabEvent());
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> getTabList({
    @required List<PointModel> points,
    @required List<OfferModel> offers,
    @required double initialZoom,
    @required List<BitmapDescriptor> markerImages,
  }) {
    return [
      _isMap
          ? HomeTab()
          : MapTab(
              key: _keyMap,
              tabController: _tabController,
              offers: offers,
              points: points,
              initialZoom: initialZoom,
              onPanelSlide: (double position) =>
                  onPanelSlide(position: position),
              markerImages: markerImages,
            ),
      PageTab(),
      AddTab(),
      NotificationTab(),
      PageTab(),
    ];
  }

  Widget get _nestAppBar {
    return Container(
      width: SizeConfig.blockSizeHorizontal * 102,
      child: AnimatedContainer(
        height: _height,
        curve: Curves.easeOut,
        duration: Duration(milliseconds: 300),
        child: Card(
          color: nestBackground,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              /*Expanded(
                child: SizedBox(
                  height: 100.0,
                ),
              ),
              Expanded(
                child: Text(
                  'Уютное гнёздышко',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: SizeConfig.safeBlockAverage * 3.5,
                  ),
                ),
              ),*/
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: TabSelector(
                    tabController: _tabController,
                    nestTabs: nestTabs,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    new Timer.periodic(const Duration(milliseconds: 1000), (Timer timer) {});
    SizeConfig().init(context);
    _tabController.addListener(() {
      (_tabController.index == 0) && !_isMap
          ? _height = 0
          : _height = _appBarHeight;
      _tabStartHeight = _height == 0 ? 0 : _height - 35.0;
      // -35 - gap from the top of app bar to beginning of the offers list
      _tabBloc.dispatch(
          UpdateTabEvent(tab: NestTabEnum.values[_tabController.index]));
      /*_tabController.index == 0
          ? _fabHeight = _initFabHeight
          : _fabHeight = 100.0;*/ //position of floating action button
      _offersBloc.state.listen((state) {
        if (state is NotLoadedOffersState) {
          setState(() {
            _fabHeight = _initFabHeight;
            /*_tabController.index == 0
                ? _fabHeight = _initFabHeight
                : _fabHeight = 20.0;*/ //position of floating action button
          });
        }
      });
    });

    return BlocBuilder(
      bloc: _tabBloc,
      builder: (BuildContext context, NestTabEnum tab) {
        if (timer != null) {
          timer.cancel();
        }
        Widget tabsBody = BlocBuilder(
          bloc: _offersBloc,
          builder: (BuildContext context, OffersState state) {
            if (state is LoadedOffersState) {
              _offers = state.offers;
              _points = state.points;
              _initialZoom = state.mapZoom;
              _markerImages = state.markerImages;
            }
            return TabBarView(
              controller: _tabController,
              children: getTabList(
                points: _points,
                offers: _offers,
                initialZoom: _initialZoom,
                markerImages: _markerImages,
              ),
              physics: physics,
            );
          },
        );

        timer = new Timer.periodic(
          const Duration(milliseconds: 100),
          (Timer timer) {
            if (_tabController.animation.value % 1 == 0) {
              timer.cancel();

              if (_keyMap.currentContext != null) {
                RenderBox box = _keyMap.currentContext.findRenderObject();
                bool isChanged = (_tabController.index != 0 &&
                            physics is NeverScrollableScrollPhysics) ||
                        (_tabController.index == 0 &&
                            physics is ClampingScrollPhysics)
                    ? true
                    : false;

                if (isChanged) {
                  setState(() {
                    if (_tabController.index == 0 &&
                        box.localToGlobal(Offset.zero).dx == 0) {
                      physics = NeverScrollableScrollPhysics();
                    } else {
                      physics = ClampingScrollPhysics();
                    }
                  });
                }
              }
            }
          },
        );

        return Material(
          child: SafeArea(
            top: true,
            bottom: true,
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                tabsBody,
                Positioned(
                  bottom: -5.0,
                  child: _nestAppBar,
                ),
                Positioned(
                  right: 20.0,
                  bottom: _fabHeight,
                  child: (_tabController.index == 0)
                      ? FloatingButton()
                      : Container(), // show floating button only on two first tabs
                ),
                _showMapButton
                    ? Positioned(
                        left: 20.0,
                        bottom: _fabHeight,
                        child: (_tabController.index == 0)
                            ? FloatingMapButton(
                                icon: _isMap
                                    ? FontAwesomeIcons.map
                                    : FontAwesomeIcons.list,
                                onPressed: switchBetweenMapAndHome,
                              )
                            : Container(),
                      )
                    : Container(),
              ],
            ),
          ),
        );
      },
    );
  }
}
