import 'dart:async';

import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:deepnesting/widgets/widgets.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:simple_moment/simple_moment.dart';

class OfferDetailsScreen extends StatefulWidget {
  final OfferModel offer;
  final SearchTabEnum searchTabEnum;
  final bool forceRefresh;

  const OfferDetailsScreen({
    Key key,
    @required this.offer,
    this.forceRefresh = false,
    this.searchTabEnum = SearchTabEnum.home,
  }) : super(key: key);

  _OfferDetailsScreenState createState() => _OfferDetailsScreenState();
}

class _OfferDetailsScreenState extends State<OfferDetailsScreen> {
  bool showContacts;
  OfferContactsBloc _offerContactsBloc;
  OffersBloc _offersBloc;
  FavoriteOffersBloc _favoriteOffersBloc;
  OfferCommentBloc _offerCommentBloc;
  LoginBloc _loginBloc;
  // ignore: cancel_subscriptions
  StreamSubscription loginBlocSubscription;
  bool showCommentEditor;
  bool isFavorite;
  TextEditingController _commentController = TextEditingController();
  FavoriteListIntBloc _favoriteListIntBloc;

  @override
  void initState() {
    super.initState();
    showContacts = false;
    _offerContactsBloc = BlocProvider.of<OfferContactsBloc>(context);
    _offersBloc = BlocProvider.of<OffersBloc>(context);
    _offersBloc
        .dispatch(AddOfferToCheckedOffersListEvent(offerId: widget.offer.id));
    if (widget.searchTabEnum == SearchTabEnum.map) {
      _offersBloc.dispatch(ClearOffersEvent());
      _offersBloc.dispatch(LoadOffersEvent());
    }
    isFavorite = false;
    _favoriteOffersBloc = BlocProvider.of<FavoriteOffersBloc>(context);
    _offerCommentBloc = BlocProvider.of<OfferCommentBloc>(context);
    _offerCommentBloc.dispatch(HideOfferCommentEvent());
    _offerCommentBloc.dispatch(ShowOfferCommentEvent(offerId: widget.offer.id));
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    showCommentEditor = false;
    _favoriteListIntBloc = BlocProvider.of<FavoriteListIntBloc>(context);
  }

  Widget _userNameAndAvatar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 15.0),
          child: Container(
            width: SizeConfig.safeBlockAverage * 7,
            child: FutureBuilder(
              future: WidgetUtils.imageCached(
                  url: WidgetUtils.getImageUrlFromImages(
                      imageURL: widget.offer.user.avatarUrl)),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return Image.file(
                    snapshot.data,
                  );
                } else {
                  return Image.asset(
                    'assets/noAvatar.jpg',
                  );
                }
              },
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              '${widget.offer.user.firstName} ${widget.offer.user.lastName}',
              maxLines: 2,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: SizeConfig.safeBlockAverage * 3,
              ),
            ),
            Text(
              widget.offer.title,
              maxLines: 5,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: SizeConfig.safeBlockAverage * 3,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _priceAndDate() {
    Moment moment = Moment.now();
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          widget.offer.price != null
              ? Flexible(
                  flex: 1,
                  child: widget.offer.price != 0
                      ? Text(
                          'Цена: ${widget.offer.price}',
                          maxLines: 2,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: SizeConfig.safeBlockAverage * 3,
                          ),
                        )
                      : Container(),
                )
              : Container(),
          Flexible(
            flex: 2,
            child: Text(
              'Размещено: ${moment.from(DateTime.parse(widget.offer.publishedAt))}',
              maxLines: 2,
              textAlign: TextAlign.end,
              style: TextStyle(
                fontSize: SizeConfig.safeBlockAverage * 3,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _offerText() {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 100,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0),
        child: Text(
          widget.offer.comment,
          style: TextStyle(
            fontSize: SizeConfig.safeBlockAverage * 3,
          ),
        ),
      ),
    );
  }

  Widget _buttonsContactsAndFavorite() {
    return BlocBuilder(
      bloc: _favoriteListIntBloc,
      builder: (BuildContext context, FavoriteListIntState state) {
        if (state is GetFavoriteListIntState) {
          isFavorite = state.favoriteList.contains(widget.offer.id);
        }
        return ButtonBar(
          children: <Widget>[
            isFavorite
                ? IconButton(
                    icon: Icon(
                      Icons.favorite,
                      color: Colors.red,
                    ),
                    onPressed: () {
                      _favoriteOffersBloc.dispatch(ClearFavoriteOffersEvent());
                      _favoriteOffersBloc.dispatch(FetchFavoriteOffersEvent());
                      _favoriteListIntBloc.dispatch(
                        RemoveFromFavoriteListIntEvent(
                            offerId: widget.offer.id),
                      );
                      if (widget.forceRefresh) {
                        _offersBloc.dispatch(ClearOffersEvent());
                        _offersBloc.dispatch(LoadOffersEvent());
                      }
                    },
                  )
                : IconButton(
                    icon: Icon(
                      Icons.favorite,
                      color: Colors.grey,
                    ),
                    onPressed: () {
                      _favoriteOffersBloc.dispatch(ClearFavoriteOffersEvent());
                      _favoriteOffersBloc.dispatch(FetchFavoriteOffersEvent());
                      _favoriteListIntBloc.dispatch(
                        AddToFavoriteListIntEvent(offerId: widget.offer.id),
                      );
                      if (widget.forceRefresh) {
                        _offersBloc.dispatch(ClearOffersEvent());
                        _offersBloc.dispatch(LoadOffersEvent());
                      }
                    },
                  ),
            showContacts
                ? FlatButton(
                    child: Text(
                      'Скрыть контакты',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: SizeConfig.safeBlockAverage * 3,
                      ),
                    ),
                    color: Colors.grey[200],
                    onPressed: () {
                      setState(
                        () {
                          showContacts = false;
                        },
                      );
                    },
                  )
                : RaisedButton(
                    child: Text(
                      'Показать контакты',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: SizeConfig.safeBlockAverage * 3),
                    ),
                    onPressed: () {
                      setState(
                        () {
                          _offerContactsBloc.dispatch(ClearContactsEvent());
                          _offerContactsBloc.dispatch(
                              LoadContactsEvent(offerId: widget.offer.id));
                          showContacts = true;
                        },
                      );
                    },
                  ),
          ],
        );
      },
    );
  }

  Widget _showContacts() {
    if (showContacts) {
      return BlocBuilder(
        bloc: _offerContactsBloc,
        builder: (BuildContext context, OfferContactsState state) {
          if (state is LoadedContactsState) {
            return Container(
              width: SizeConfig.safeBlockHorizontal * 100,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: const ClampingScrollPhysics(),
                  itemCount: state.contactsList.length,
                  itemBuilder: (BuildContext context, int index) {
                    OfferContactsModel contact = state.contactsList[index];
                    bool ifLink = contact.type == 'link';
                    return RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: contact.text,
                            style: TextStyle(
                                fontSize: SizeConfig.safeBlockAverage * 3,
                                color: ifLink ? Colors.blue : Colors.black),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                if (ifLink) {
                                  WidgetUtils.redirectToUrl(contact.value);
                                }
                              },
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
            );
          } else {
            return NestProgressIndicator();
          }
        },
      );
    } else {
      return Container();
    }
  }

  Widget _commentsList() {
    _loginBloc.state.listen((state) {
      if (state is LogInState) {
        setState(() {
          showCommentEditor = true;
        });
      } else {
        setState(() {
          showCommentEditor = false;
        });
      }
    });
    return Container(
      width: SizeConfig.blockSizeHorizontal * 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              'Комментарии',
              textAlign: TextAlign.right,
              style: TextStyle(
                fontSize: SizeConfig.safeBlockAverage * 3.5,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          BlocBuilder(
            bloc: _offerCommentBloc,
            builder:
                (BuildContext context, OfferCommentState offerCommentState) {
              if (offerCommentState is ShowOfferCommentState) {
                return ListView.builder(
                  shrinkWrap: true,
                  physics: const ClampingScrollPhysics(),
                  itemCount: offerCommentState.commentsList.length,
                  itemBuilder: (BuildContext context, int index) {
                    OfferCommentsModel comment =
                        offerCommentState.commentsList[index];
                    String fullAvatarUrl = WidgetUtils.getImageUrlFromImages(
                        imageURL: comment.user.avatarURL);
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 15.0,top: 5.0, bottom: 5.0),
                          child: Container(
                            width: SizeConfig.safeBlockAverage * 7,
                            child: FutureBuilder(
                              future:
                                  WidgetUtils.imageCached(url: fullAvatarUrl),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (snapshot.hasData) {
                                  return Image.file(
                                    snapshot.data,
                                  );
                                } else {
                                  return Image.asset(
                                    'assets/noAvatar.jpg',
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              comment.user.name,
                              maxLines: 2,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: SizeConfig.safeBlockAverage * 3,
                              ),
                            ),
                            Text(
                              comment.text,
                              maxLines: 5,
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: SizeConfig.safeBlockAverage * 3,
                              ),
                            ),
                          ],
                        ),
                      ],
                    );
                  },
                );
              } else if (offerCommentState is ErrorOfferCommentState) {
                return Center(
                  child: Text(
                    'Возникла ошибка ${offerCommentState.error}',
                    style: TextStyle(
                      fontSize: SizeConfig.safeBlockAverage * 3,
                    ),
                  ),
                );
              } else {
                return Container();
              }
            },
          ),
          showCommentEditor
              ? Column(
                  children: <Widget>[
                    Container(
                      child: TextField(
                        controller: _commentController,
                        decoration: InputDecoration(
                            labelText: 'Введите ваш комментарий'),
                      ),
                    ),
                    ButtonBar(
                      children: <Widget>[
                        RaisedButton(
                          child: Text(
                            'Отправить комментарий',
                            style: TextStyle(
                              fontSize: SizeConfig.safeBlockAverage * 3,
                              color: Colors.white,
                            ),
                          ),
                          onPressed: () {
                            _offerCommentBloc.dispatch(
                              SendOfferCommentEvent(
                                offerId: widget.offer.id,
                                comment: _commentController.text,
                              ),
                            );
                            _commentController.text = '';
                          },
                        ),
                      ],
                    ),
                  ],
                )
              : Container()
        ],
      ),
    );
  }

  Widget _offerImages() {
    List<String> images =
        WidgetUtils.getFullSizeImages(images: widget.offer.images);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        images.length != 1
            ? StaggeredGridView.countBuilder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                crossAxisCount: 4,
                itemCount: images.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => HeroPhotoViewWrapper(
                            firstPage: index,
                            imageList: images,
                          ),
                        ),
                      );
                    },
                    child: Container(
                      child: Hero(
                        tag: 'image${widget.offer.user.avatarUrl}$index',
                        child: FutureBuilder(
                          future: WidgetUtils.imageCached(url: images[index]),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              return Image.file(
                                snapshot.data,
                              );
                            } else {
                              return Image.asset(
                                'assets/noPhoto.jpeg',
                              );
                            }
                          },
                        ),
                      ),
                    ),
                  );
                },
                staggeredTileBuilder: (int index) => StaggeredTile.fit(2),
                mainAxisSpacing: 4.0,
                crossAxisSpacing: 4.0,
              )
            : Container(
                width: SizeConfig.safeBlockHorizontal * 100,
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => HeroPhotoViewWrapper(
                          firstPage: 0,
                          imageList: images,
                        ),
                      ),
                    );
                  },
                  child: Container(
                    child: Hero(
                      tag: 'image${widget.offer.user.avatarUrl}0',
                      child: FutureBuilder(
                        future: WidgetUtils.imageCached(url: images[0]),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            return Image.file(
                              snapshot.data,
                            );
                          } else {
                            return Image.asset(
                              'assets/noPhoto.jpeg',
                            );
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.offer.title,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _userNameAndAvatar(),
                _priceAndDate(),
                _offerText(),
                _buttonsContactsAndFavorite(),
                _showContacts(),
                SizedBox(
                  height: SizeConfig.safeBlockAverage * 5,
                ),
                _offerImages(),
                _commentsList(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
