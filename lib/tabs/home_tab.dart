import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/utils/colors.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:deepnesting/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:deepnesting/models/models.dart';

class HomeTab extends StatefulWidget {
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    final OffersBloc _offersBloc = BlocProvider.of<OffersBloc>(context);
    return Container(
      color: nestGray,
      child: BlocBuilder(
        bloc: _offersBloc,
        builder: (BuildContext context, OffersState state) {
          if (state is LoadedOffersState) {
            if (state.offers.length != 0) {
              return RefreshIndicator(
                onRefresh: () async {
                  _offersBloc.dispatch(ClearOffersEvent());
                  _offersBloc.dispatch(LoadOffersEvent());
                },
                child: ListView.builder(
                    shrinkWrap: true,
                    physics: const ClampingScrollPhysics(),
                    itemCount: state.offers.length,
                    itemBuilder: (BuildContext context, int index) {
                      final OfferModel offer = state.offers[index];
                      final bool checked =
                          state.checkedOffers.contains(state.offers[index].id);
                      return OfferCard(
                        offer: offer,
                        checked: checked,
                      );
                    }),
              );
            } else {
              return Container(
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          'По вашему запросу ничего не найдено',
                          maxLines: 3,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.safeBlockAverage * 4,
                          ),
                        ),
                      ),
                      Container(
                        width: SizeConfig.safeBlockHorizontal * 70,
                        child: RaisedButton(
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Сбросить результаты поиска',
                              maxLines: 4,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: nestMainColor,
                                fontSize: SizeConfig.safeBlockAverage * 4,
                              ),
                            ),
                          ),
                          onPressed: () {
                            //_offersBloc.dispatch(ClearOffersEvent());
                            _offersBloc.dispatch(LoadOffersEvent(
                              priceFrom: null,
                              priceTo: null,
                              searchType: null,
                              buildType: null,
                            ));
                          },
                        ),
                      )
                    ],
                  ),
                ),
              );
            }
          } else {
            return Center(
              child: NestProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
