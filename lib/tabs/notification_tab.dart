import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/utils/colors.dart';
import 'package:deepnesting/utils/size_config.dart';
import 'package:deepnesting/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NotificationTab extends StatefulWidget {
  _NotificationTabState createState() => _NotificationTabState();
}

class _NotificationTabState extends State<NotificationTab> with AutomaticKeepAliveClientMixin {
  LoginBloc _loginBloc;

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      color: nestGray,
      child: Padding(
        padding: const EdgeInsets.only(top: 30.0),
        child: BlocBuilder(
          bloc: _loginBloc,
          builder: (BuildContext context, LoginState state) {
            if (state is LogOutState) {
              return LoginWindow();
            } else if (state is LogInState) {
              return NotificationWindow();
            } else if (state is ShowRegisterPageState) {
              return RegisterWindow();
            } else if (state is WaitingForLoginState) {
              return Center(
                child: NestProgressIndicatorWhite(),
              );
            } else if (state is ErrorLoginState){
              return Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'Произошла ошибка: ${state.error}',
                      maxLines: 6,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: SizeConfig.safeBlockAverage * 3,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        child: Text(
                          'Вернуться на страницу логина',
                          maxLines: 2,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.safeBlockAverage * 3,
                          ),
                        ),
                        onPressed: () {
                          _loginBloc.dispatch(LogOutEvent());
                        },
                      ),
                    )
                  ],
                ),
              );
            } else {
              return Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'Произошла ошибка',
                      maxLines: 6,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: SizeConfig.safeBlockAverage * 3,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                        child: Text(
                          'Вернуться на страницу логина',
                          maxLines: 2,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.safeBlockAverage * 3,
                          ),
                        ),
                        onPressed: () {
                          _loginBloc.dispatch(LogOutEvent());
                        },
                      ),
                    )
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
