import 'dart:async';
import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/screens/screens.dart';
import 'package:deepnesting/tabs/tabs.dart';
import 'package:deepnesting/utils/colors.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:deepnesting/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:dio/dio.dart';

class MapTab extends StatefulWidget {
  final TabController tabController;
  final List<PointModel> points;
  final List<OfferModel> offers;
  final double initialZoom;
  final ValueChanged<double> onPanelSlide;
  final List<BitmapDescriptor> markerImages;

  const MapTab({
    Key key,
    @required this.tabController,
    @required this.points,
    @required this.offers,
    @required this.onPanelSlide,
    @required this.initialZoom,
    @required this.markerImages,
  }) : super(key: key);

  _MapTabState createState() => _MapTabState();
}

class _MapTabState extends State<MapTab> with AutomaticKeepAliveClientMixin {
  Completer<GoogleMapController> _controller = Completer();
  static const LatLng _center = const LatLng(59.929718, 30.310805);
  final Set<Marker> _markers = {};
  final PanelController _panelController = PanelController();
  MainPageOffersBloc _mainPageOffersBloc;
  OffersBloc _offersBloc;
  double initialMapZoom;
  Duration timerDuration = const Duration(seconds: 1);
  double oldZoom;
  double changedZoom;
  BitmapDescriptor myIcon;
  Map<MarkerId, Marker> markers = Map();
  CancelToken cancelToken = CancelToken();
  List<int> checkedOffers = List();
  List<int> favoriteOffers = List();
  bool isMap = true;

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  void _onGeoChanged(CameraPosition position) {
    changedZoom = position.zoom;
  }

  void _onCameraIdle() {
    if (changedZoom != oldZoom && changedZoom != initialMapZoom) {
      //_offersBloc.dispatch(ClearOffersEvent());
      if (cancelToken != null && !cancelToken.isCancelled) {
        cancelToken.cancel();
      }
      cancelToken = CancelToken();
      _offersBloc.dispatch(LoadOffersEvent(
          mapZoom: changedZoom, onlyZoom: true, cancelToken: cancelToken));
      oldZoom = changedZoom;
    }
  }

  switchMapHomeTabs() {
    setState(() {
      isMap = !isMap;
    });
  }

  addMarkers({@required List<PointModel> points}) async {
    for (int i = 0; i < points.length; i++) {
      _markers.add(
        Marker(
          markerId: MarkerId(LatLng(points[i].lat, points[i].lon).toString()),
          position: LatLng(
              points[i].lat != 0 ? points[i].lat : _center.latitude,
              points[i].lon != 0 ? points[i].lon : _center.longitude),
          icon: widget.markerImages[i],
          onTap: () {
            _mainPageOffersBloc.dispatch(ClearMainPageOffersEvent());
            if (points[i].type == 'point') {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OfferDetailsScreen(
                    offer: modelById(id: widget.offers[i].id),
                    searchTabEnum:
                        isMap ? SearchTabEnum.map : SearchTabEnum.home,
                  ),
                ),
              );
            } else {
              _mainPageOffersBloc.dispatch(
                  LoadSpecificMainPageOffersEvent(offersIds: points[i].items));
              _panelController.open();
            }
          },
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _mainPageOffersBloc = BlocProvider.of<MainPageOffersBloc>(context);
    _offersBloc = BlocProvider.of<OffersBloc>(context);
    initialMapZoom = widget.initialZoom;
    addMarkers(points: widget.points);
  }

  OfferModel modelById({@required int id}) {
    return widget.offers.firstWhere((offer) => offer.id == id);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (isMap) {
      return SlidingUpPanel(
        controller: _panelController,
        color: Colors.transparent,
        maxHeight: HomeScreen.panelHeightOpen,
        minHeight: HomeScreen.panelHeightClosed,
        onPanelSlide: widget.onPanelSlide,
        parallaxEnabled: true,
        parallaxOffset: .5,
        panel: GestureDetector(
          onPanUpdate: (details) {
            if (this.mounted) {
              if (details.delta.dx < 0) {
                widget.tabController.animateTo(
                    NestTabEnum.values.indexOf(NestTabEnum.messages));
              }
            }
          },
          child: Padding(
            padding: const EdgeInsets.only(right: 5.0, left: 5.0, bottom: 20.0),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
              color: nestGray,
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 70.0, bottom: 25.0),
                    child: BlocBuilder(
                      bloc: _mainPageOffersBloc,
                      builder:
                          (BuildContext context, MainPageOffersState state) {
                        List<OfferModel> _offers;
                        if (state is MainPageLoadOffersState) {
                          _offers = state.offers;
                          checkedOffers = state.checkedOffers;
                          favoriteOffers = state.favoriteOffers;
                        } else {
                          _offers = widget.offers;
                        }
                        return ListView.builder(
                          shrinkWrap: true,
                          physics: const ClampingScrollPhysics(),
                          itemCount: _offers.length,
                          itemBuilder: (BuildContext context, int index) {
                            final OfferModel offer = _offers[index];
                            final bool checked =
                                checkedOffers.contains(_offers[index].id);
                            return OfferCard(
                              offer: offer,
                              checked: checked,
                            );
                          },
                        );
                      },
                    ),
                  ),
                  Container(
                    height: 55.0,
                    child: Center(
                      child: Text(
                        'Объявления',
                        style: TextStyle(
                          fontSize: SizeConfig.safeBlockAverage * 3,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: SizeConfig.safeBlockHorizontal * 100,
                    height: 18.0,
                    child: Center(
                      child: Container(
                        height: 4.0,
                        width: 32.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            color: nestDarkGray),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: Center(
          child: BlocBuilder(
            bloc: _offersBloc,
            builder: (BuildContext context, OffersState state) {
              if (state is LoadedOffersState) {
                initialMapZoom = state.mapZoom;
                _markers.clear();
                //change points
                addMarkers(points: state.points);
              }
              return GoogleMap(
                onMapCreated: _onMapCreated,
                initialCameraPosition: CameraPosition(
                  target: _center,
                  zoom: initialMapZoom,
                ),
                markers: _markers,
                onCameraMove: _onGeoChanged,
                onCameraIdle: _onCameraIdle,
              );
            },
          ),
        ),
      );
    } else {
      return HomeTab();
    }
  }

  @override
  bool get wantKeepAlive => true;
}
