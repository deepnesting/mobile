import 'package:flutter/material.dart';

class TabSelector extends StatelessWidget with PreferredSizeWidget {
  final TabController tabController;
  final List<Tab> nestTabs;

  TabSelector(
      {Key key,
      @required this.tabController,
      @required this.nestTabs,
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TabBar(
      controller: tabController,
      tabs: nestTabs,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kTextTabBarHeight);
}
