import 'package:deepnesting/utils/size_config.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:flutter/material.dart';

class ToggleButtons2 extends StatefulWidget {
  final void Function(int index) onPressed;
  final List<String> items;
  final List<bool> isSelected;
  final double fontMultiplier;
  final Color neutralColor;
  final Color activeColor;
  final Color borderColor;

  ToggleButtons2({
    Key key,
    this.onPressed,
    @required this.items,
    this.isSelected,
    this.fontMultiplier = 2.7,
    this.neutralColor = nestCyan,
    this.activeColor = nestGrayBackgroundButtons,
    this.borderColor = nestGrayBackgroundButtons,
  });

  @override
  _ToggleButtons2State createState() =>
      _ToggleButtons2State(this.items, this.isSelected);
}

class _ToggleButtons2State extends State<ToggleButtons2> {
  List<bool> active;
  List<String> items;

  _ToggleButtons2State(List<String> items, List<bool> isSelected) {
    this.items = items;
    active = List<bool>.generate(
      items.length,
      (int index) {
        return false;
      },
    );
    if (isSelected != null) {
      active = isSelected;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: new BorderRadius.all(new Radius.circular(20.0)),
        color: widget.borderColor,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: List<Widget>.generate(
          items.length,
          (int index) {
            bool isActive = false;
            if (active[index] != null) {
              isActive = active[index];
            }
            return Expanded(
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      active = List<bool>.generate(items.length, (int index) {
                        return false;
                      });
                      active[index] = true;
                      widget.onPressed(index);
                    });
                  },
                  child: Container(
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          items[index],
                          style: TextStyle(
                            color: isActive ? Colors.white : nestGrayIcons,
                              fontSize: SizeConfig.safeBlockAverage *
                                  widget.fontMultiplier),
                        ),
                      ),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                        color: isActive
                            ? widget.neutralColor
                            : widget.activeColor,
                    ),
                  )),
            );
          },
        ),
      ),
    );
  }
}
