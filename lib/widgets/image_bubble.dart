import 'package:deepnesting/utils/utils.dart';
import 'package:flutter/material.dart';

class ImageBubble extends StatelessWidget {
  final int index;
  final int length;

  const ImageBubble({Key key, @required this.index, @required this.length})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool ifShow = SizeConfig.ifVertical
        ? (index == 5) && (length - 6 != 0)
        : (index == 8) && (length - 9 != 0);
    return Positioned.fill(
      child: Align(
        child: Container(
          height: SizeConfig.safeBlockAverage * 8,
          width: SizeConfig.safeBlockAverage * 8,
          decoration: ifShow
              ? BoxDecoration(shape: BoxShape.circle, color: nestCyan)
              : null,
          child: Center(
            child: ifShow
                ? Text(
                    SizeConfig.ifVertical
                        ? 'еще ${length - 6}'
                        : 'еще ${length - 9}',
                    style: TextStyle(
                      fontSize: SizeConfig.safeBlockAverage * 2,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                    maxLines: 2,
                  )
                : Container(),
          ),
        ),
        alignment: Alignment.center,
      ),
    );
  }
}
