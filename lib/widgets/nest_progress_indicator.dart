import 'package:deepnesting/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class NestProgressIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SpinKitCircle(
      color: nestMainColor,
      size: SizeConfig.safeBlockAverage * 7,
    );
  }
}