import 'package:deepnesting/widgets/widgets.dart';
import 'package:deepnesting/utils/widget_utils.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:swipedetector/swipedetector.dart';

class HeroPhotoViewWrapper extends StatelessWidget {
  final Widget loadingChild;
  final Decoration backgroundDecoration;
  final List<String> imageList;
  final int firstPage;

  const HeroPhotoViewWrapper(
      {this.loadingChild,
      this.backgroundDecoration,
      @required this.imageList,
      @required this.firstPage});

  @override
  Widget build(BuildContext context) {
    PageController _pageController = PageController(initialPage: firstPage);
    return SwipeDetector(
      onSwipeUp: () {
        Navigator.of(context).pop();
      },
      child: Container(
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: PhotoViewGallery.builder(
          itemCount: imageList.length,
          builder: (context, index) {
            return PhotoViewGalleryPageOptions(
              imageProvider: NetworkImage(
                  WidgetUtils.getFullSizeImageURL(imageURL: imageList[index])),
              minScale: PhotoViewComputedScale.contained,
              maxScale: PhotoViewComputedScale.covered * 1.1,
            );
          },
          pageController: _pageController,
          scrollPhysics: ClampingScrollPhysics(),
          backgroundDecoration: BoxDecoration(
            color: Colors.black,
          ),
          loadingChild: Center(
            child: NestProgressIndicator(),
          ),
          onPageChanged: (int index) {
            if (imageList.length - 1 <= index) {
              return;
            }
            precacheImage(
                NetworkImage(WidgetUtils.getFullSizeImageURL(
                    imageURL: imageList[index + 1])),
                context);
            if (index > 0) {
              precacheImage(
                  NetworkImage(WidgetUtils.getFullSizeImageURL(
                      imageURL: imageList[index - 1])),
                  context);
            }
          },
        ),
      ),
    );
  }
}
