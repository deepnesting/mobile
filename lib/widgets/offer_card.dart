import 'dart:async';

import 'package:deepnesting/blocs/blocks.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_moment/simple_moment.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/screens/screens.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:deepnesting/widgets/widgets.dart';

class OfferCard extends StatefulWidget {
  final OfferModel offer;
  final bool checked;
  final bool forceRefresh;

  const OfferCard({
    Key key,
    @required this.offer,
    this.checked = false,
    this.forceRefresh = false,
  }) : super(key: key);

  _OfferCardState createState() => _OfferCardState();
}

class _OfferCardState extends State<OfferCard> {
  bool ifTextIsOpen;
  String comment;
  bool showContacts;
  OfferContactsBloc _offerContactsBloc;
  OffersBloc _offersBloc;
  FavoriteListIntBloc _favoriteListIntBloc;
  ModerationBloc _moderationBloc;
  bool isFavorite;
  OfferStatusEnum _offerStatusEnum;
  LoginBloc loginBloc;
  StreamSubscription loginSubscription;
  bool allowModeration;
  bool isModerator;

  @override
  void initState() {
    super.initState();
    ifTextIsOpen = false;
    comment =
        '${widget.offer.comment.split(' ').take(10).toList().join(' ')}...';
    showContacts = false;
    _offerContactsBloc = BlocProvider.of<OfferContactsBloc>(context);
    _offersBloc = BlocProvider.of<OffersBloc>(context);
    _favoriteListIntBloc = BlocProvider.of<FavoriteListIntBloc>(context);
    _moderationBloc = BlocProvider.of<ModerationBloc>(context);
    isFavorite = false;
    _offerStatusEnum = selectOfferStatus(statusId: widget.offer.statusId);
    loginBloc = BlocProvider.of<LoginBloc>(context);
    allowModeration = false;
    isModerator = false;
    loginSubscription = loginBloc.state.listen((state) {
      if (state is LogInState) {
        if (state.user.isModerator == true ||
            widget.offer.user.id == state.user.userId) {
          allowModeration = true;
          isModerator = state.user.isModerator;
        } else {
          allowModeration = false;
          isModerator = false;
        }
      } else {
        allowModeration = false;
        isModerator = false;
      }
    });
  }

  // select a status of an offer
  OfferStatusEnum selectOfferStatus({@required int statusId}) {
    switch (statusId) {
      case 10:
        return OfferStatusEnum.draft;
        break;
      case 11:
        return OfferStatusEnum.moderation;
        break;
      case 12:
        return OfferStatusEnum.published;
        break;
      default:
        return OfferStatusEnum.draft;
        break;
    }
  }

  onOfferTap() {
    if (ifTextIsOpen) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => OfferDetailsScreen(
            offer: widget.offer,
            forceRefresh: widget.forceRefresh,
          ),
        ),
      );
    } else {
      setState(() {
        comment = widget.offer.comment;
        ifTextIsOpen = true;
      });
    }
  }

  Container contacts(LoadedContactsState state) {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 100,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: ListView.builder(
          shrinkWrap: true,
          physics: const ClampingScrollPhysics(),
          itemCount: state.contactsList.length,
          itemBuilder: (BuildContext context, int index) {
            OfferContactsModel contact = state.contactsList[index];
            bool ifLink = contact.type == 'link';
            return RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: contact.text,
                    style: TextStyle(
                        fontSize: SizeConfig.safeBlockAverage * 3,
                        color: ifLink ? Colors.blue : Colors.black),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        if (ifLink) {
                          WidgetUtils.redirectToUrl(contact.value);
                        }
                      },
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  ButtonBar buildButtonBar() {
    return ButtonBar(
      children: <Widget>[
        isFavorite
            ? IconButton(
                icon: Icon(
                  Icons.favorite,
                  color: Colors.red,
                ),
                onPressed: () {
                  if (widget.forceRefresh) {
                    _offersBloc.dispatch(
                      ClearOffersEvent(),
                    );
                    _offersBloc.dispatch(
                      LoadOffersEvent(),
                    );
                  }
                  _favoriteListIntBloc.dispatch(
                    RemoveFromFavoriteListIntEvent(offerId: widget.offer.id),
                  );
                },
              )
            : IconButton(
                icon: Icon(
                  Icons.favorite,
                  color: Colors.grey,
                ),
                onPressed: () {
                  if (widget.forceRefresh) {
                    _offersBloc.dispatch(
                      ClearOffersEvent(),
                    );
                    _offersBloc.dispatch(
                      LoadOffersEvent(),
                    );
                  }
                  _favoriteListIntBloc.dispatch(
                    AddToFavoriteListIntEvent(offerId: widget.offer.id),
                  );
                },
              ),
        showContacts
            ? FlatButton(
                child: Text(
                  'Скрыть контакты',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: SizeConfig.safeBlockAverage * 3,
                  ),
                ),
                color: Colors.grey[200],
                onPressed: () {
                  setState(() {
                    showContacts = false;
                  });
                },
              )
            : RaisedButton(
                child: Text(
                  'Показать контакты',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: SizeConfig.safeBlockAverage * 3,
                  ),
                ),
                onPressed: () {
                  setState(() {
                    _offerContactsBloc.dispatch(
                      ClearContactsEvent(),
                    );
                    _offerContactsBloc.dispatch(
                      LoadContactsEvent(offerId: widget.offer.id),
                    );
                    showContacts = true;
                  });
                },
              ),
      ],
    );
  }

  Container offerComment() {
    return Container(
      width: SizeConfig.safeBlockHorizontal * 100,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0),
        child: Text(
          comment,
          style: TextStyle(
            fontSize: SizeConfig.safeBlockAverage * 3,
          ),
        ),
      ),
    );
  }

  BlocBuilder<FavoriteListIntBloc, FavoriteListIntState> contactsButton() {
    return BlocBuilder(
      bloc: _favoriteListIntBloc,
      builder: (BuildContext context, FavoriteListIntState state) {
        if (state is GetFavoriteListIntState) {
          isFavorite = state.favoriteList.contains(widget.offer.id);
        }
        return buildButtonBar();
      },
    );
  }

  Widget contactsInformation() {
    return showContacts
        ? BlocBuilder(
            bloc: _offerContactsBloc,
            builder: (BuildContext context, OfferContactsState state) {
              if (state is LoadedContactsState) {
                return contacts(state);
              } else {
                return NestProgressIndicator();
              }
            },
          )
        : Container();
  }

  Row userInformation() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 15.0),
          child: Container(
            width: SizeConfig.safeBlockAverage * 7,
            child: FutureBuilder(
              future: WidgetUtils.imageCached(
                url: WidgetUtils.getImageUrlFromImages(
                    imageURL: widget.offer.user.avatarUrl),
              ),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  return Image.file(
                    snapshot.data,
                  );
                } else {
                  return Image.asset(
                    'assets/noAvatar.jpg',
                  );
                }
              },
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              '${widget.offer.user.firstName} ${widget.offer.user.lastName}',
              maxLines: 2,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: SizeConfig.safeBlockAverage * 3,
              ),
            ),
            Text(
              widget.offer.title,
              maxLines: 5,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: SizeConfig.safeBlockAverage * 3,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Padding offerPriceAndDate(Moment moment, DateTime publishedAt) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          widget.offer.price != null
              ? Flexible(
                  flex: 1,
                  child: widget.offer.price != 0
                      ? Text(
                          'Цена: ${widget.offer.price}',
                          maxLines: 2,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: SizeConfig.safeBlockAverage * 3,
                          ),
                        )
                      : Container(),
                )
              : Container(),
          Flexible(
            flex: 2,
            child: Text(
              'Размещено: ${moment.from(publishedAt)}',
              maxLines: 2,
              textAlign: TextAlign.end,
              style: TextStyle(
                fontSize: SizeConfig.safeBlockAverage * 3,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget moderationInformation() {
    return BlocBuilder(
      bloc: _moderationBloc,
      builder: (BuildContext context, ModerationState state) {
        String error;
        if (state is SuccessfullySubmittedModerationState) {
          _offerStatusEnum = OfferStatusEnum.moderation;
        } else if (state is SuccessfullyWithdrawModerationState) {
          _offerStatusEnum = OfferStatusEnum.draft;
        } else if (state is ErrorModerationState) {
          _offerStatusEnum = OfferStatusEnum.error;
          error = state.error;
        } else if (state is PublishModerationState) {
          _offerStatusEnum = OfferStatusEnum.published;
        }

        switch (_offerStatusEnum) {
          case OfferStatusEnum.draft:
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Черновик',
                  maxLines: 2,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: SizeConfig.safeBlockAverage * 3,
                  ),
                ),
                RaisedButton(
                  child: Text(
                    'Отправить на модерацию',
                    maxLines: 2,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: SizeConfig.safeBlockAverage * 3,
                    ),
                  ),
                  onPressed: () {
                    _moderationBloc.dispatch(
                      SendToModerationEvent(offerId: widget.offer.id),
                    );

                    /*setState(() {
                      _offerStatusEnum = OfferStatusEnum.moderation;
                    });*/
                  },
                ),
              ],
            );
            break;
          case OfferStatusEnum.moderation:
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Отправлено на модерацию',
                  maxLines: 2,
                  style: TextStyle(
                    fontSize: SizeConfig.safeBlockAverage * 3,
                  ),
                ),
                isModerator
                    ? RaisedButton(
                        child: Text(
                          'Опубликовать',
                          maxLines: 2,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.safeBlockAverage * 3,
                          ),
                        ),
                        onPressed: () {
                          _moderationBloc.dispatch(
                            PublishModerationEvent(offerId: widget.offer.id),
                          );
                        },
                      )
                    : Container(),
              ],
            );
            break;
          case OfferStatusEnum.published:
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Опубликовано',
                  maxLines: 2,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontSize: SizeConfig.safeBlockAverage * 3,
                  ),
                ),
                RaisedButton(
                  child: Text(
                    'Снять с публикации',
                    maxLines: 2,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: SizeConfig.safeBlockAverage * 3,
                    ),
                  ),
                  onPressed: () {
                    _moderationBloc.dispatch(
                      WithdrawModerationEvent(offerId: widget.offer.id),
                    );
                  },
                ),
              ],
            );
            break;
          case OfferStatusEnum.error:
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  error,
                  maxLines: 2,
                  style: TextStyle(
                    fontSize: SizeConfig.safeBlockAverage * 3,
                  ),
                ),
              ],
            );
            break;
          default:
            return Container();
            break;
        }
      },
    );
  }

  Card offerCard(Moment moment, DateTime publishedAt, List<String> images) {
    return Card(
      color: widget.checked ? nestDarkGray : Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            InkWell(
              onTap: () {
                onOfferTap();
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  userInformation(),
                  offerPriceAndDate(moment, publishedAt),
                  allowModeration ? moderationInformation() : Container(),
                  contactsButton(),
                  contactsInformation(),
                  offerComment(),
                ],
              ),
            ),
            OfferImages(
              images: images,
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Moment moment = Moment.now();
    DateTime publishedAt = DateTime.parse(widget.offer.publishedAt);
    List<String> images =
        WidgetUtils.getImagesWithUrl(images: widget.offer.images);
    return Container(
      width: SizeConfig.safeBlockHorizontal * 90,
      child: offerCard(moment, publishedAt, images),
    );
  }
}
