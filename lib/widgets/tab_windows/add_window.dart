import 'dart:io';

import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:validators/validators.dart';

import '../widgets.dart';

class AddWindow extends StatefulWidget {
  _AddWindowState createState() => _AddWindowState();
}

class _AddWindowState extends State<AddWindow> {
  final _addressController = TextEditingController();
  final _commentController = TextEditingController();
  final _priceController = TextEditingController();
  final _addOfferFormKey = GlobalKey<FormState>();
  AddOfferBloc _addOfferBloc;
  int _nestSearchType;
  int _nestBuildType;
  int _nestRoomCount;
  List<bool> searchTypeSelected = [false, false];
  List<bool> buildTypeSelected = [false, false];
  List<bool> roomCountSelected = [false, false, false, false];
  List<int> searchTypeOptions = [1, 2];
  List<int> buildTypeOptions = [0, 1];
  List<int> roomCountOptions = [4, 1, 2, 3];
  List<File> images = List();

  @override
  void initState() {
    super.initState();
    _addOfferBloc = BlocProvider.of<AddOfferBloc>(context);
    _addressController.text = '';
    _commentController.text = '';
    _priceController.text = '';
    searchTypeSelected =
        searchTypeOptions.map((option) => option == _nestSearchType).toList();
    buildTypeSelected =
        buildTypeOptions.map((option) => option == _nestBuildType).toList();
    roomCountSelected =
        roomCountOptions.map((option) => option == _nestRoomCount).toList();
  }

  @override
  void dispose() {
    _addressController.dispose();
    _commentController.dispose();
    _priceController.dispose();
    super.dispose();
  }

  Future getImage() async {
    File tempImage = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (tempImage != null) {
      setState(() {
        images.add(tempImage);
      });
    }
  }

  removeImage({@required int index}) {
    setState(() {
      images.removeAt(index);
    });
  }

  Widget imagesList() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: GridView.builder(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemCount: images.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: WidgetUtils.ifVertical() ? 3 : 6,
            childAspectRatio: 1.3,
          ),
          itemBuilder: (BuildContext context, int index) {
            return Stack(
              children: <Widget>[
                Container(
                  width: SizeConfig.safeBlockAverage * 40,
                  height: SizeConfig.safeBlockAverage * 40,
                  child: Image.file(
                    images[index],
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  top: 5.0,
                  right: 5.0,
                  child: InkWell(
                    child: Card(
                      child: Center(
                        child: Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                      ),
                      color: nestMainColor,
                    ),
                    onTap: () {
                      removeImage(index: index);
                    },
                  ),
                )
              ],
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Card(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    'Добавить объявление',
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: SizeConfig.safeBlockAverage * 3,
                    ),
                  ),
                ),
              ),
            ),
            BlocBuilder(
                bloc: _addOfferBloc,
                builder: (BuildContext context, AddOfferState state) {
                  if (state is InitialAddOfferState) {
                    return Form(
                      key: _addOfferFormKey,
                      child: Card(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ButtonBar(
                              children: <Widget>[
                                RaisedButton(
                                  child: Text(
                                    'Опубликовать',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize:
                                            SizeConfig.safeBlockAverage * 3),
                                  ),
                                  onPressed: () {
                                    if (_addOfferFormKey.currentState
                                        .validate()) {
                                      _addOfferBloc
                                          .dispatch(LoadingAddOfferEvent());
                                      _addOfferBloc.dispatch(SendAddOfferEvent(
                                        searchType: _nestSearchType,
                                        buildType: _nestBuildType,
                                        roomCount: _nestRoomCount,
                                        comment: _commentController.text,
                                        price: int.parse(_priceController.text),
                                        address: _addressController.text,
                                        images: images,
                                      ));
                                    }
                                  },
                                ),
                              ],
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 10.0),
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 90,
                                child: Text(
                                  'Тип объявления',
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15.0),
                              child: ToggleButtons2(
                                onPressed: (int index) {
                                  setState(() {
                                    for (int buttonIndex = 0;
                                        buttonIndex < searchTypeSelected.length;
                                        buttonIndex++) {
                                      if (buttonIndex == index) {
                                        searchTypeSelected[buttonIndex] = true;
                                        _nestSearchType = index + 1;
                                      } else {
                                        searchTypeSelected[buttonIndex] = false;
                                      }
                                    }
                                  });
                                },
                                items: ['Ищу гнёздышко', 'Ищу гнездоискателя'],
                                isSelected: searchTypeSelected,
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 10.0),
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 90,
                                child: Text(
                                  'Тип жилья',
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15.0),
                              child: ToggleButtons2(
                                onPressed: (int index) {
                                  setState(() {
                                    for (int buttonIndex = 0;
                                        buttonIndex < buildTypeSelected.length;
                                        buttonIndex++) {
                                      if (buttonIndex == index) {
                                        buildTypeSelected[buttonIndex] = true;
                                        _nestBuildType = index + 1;
                                      } else {
                                        buildTypeSelected[buttonIndex] = false;
                                      }
                                    }
                                  });
                                },
                                items: ['Комнату', 'Квартиру'],
                                isSelected: buildTypeSelected,
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 10.0),
                              child: Container(
                                width: SizeConfig.safeBlockHorizontal * 90,
                                child: Text(
                                  'Количество комнат',
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15.0),
                              child: ToggleButtons2(
                                onPressed: (int index) {
                                  setState(() {
                                    for (int buttonIndex = 0;
                                        buttonIndex < roomCountSelected.length;
                                        buttonIndex++) {
                                      if (buttonIndex == index) {
                                        roomCountSelected[buttonIndex] = true;
                                        _nestRoomCount = index;
                                      } else {
                                        roomCountSelected[buttonIndex] = false;
                                      }
                                    }
                                  });
                                },
                                items: ['Студия', '1', '2', '3+'],
                                isSelected: roomCountSelected,
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15.0),
                              child: TextFormField(
                                controller: _addressController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    labelText: 'Адрес объявления'),
                                maxLines: null,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Заполните пожалуйста адрес';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15.0),
                              child: TextFormField(
                                controller: _commentController,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    labelText: 'Текст объявления'),
                                maxLines: null,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Заполните пожалуйста текст объявления';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15.0),
                              child: TextFormField(
                                controller: _priceController,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(labelText: 'Цена',hoverColor: nestGrayBackgroundButtons),
                                inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                                validator: (value) {
                                  if (value.isEmpty) {
                                    String result = 'Укажите пожалуйста цену';
                                    return result;
                                  } else if (!isNumeric(value)) {
                                    String result =
                                        'Цена должна быть указана в цифрах';
                                    return result;
                                  }
                                  return null;
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Center(
                                      child: RaisedButton(
                                        child: Text(
                                          'Добавить фото',
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                        onPressed: () {
                                          getImage();
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            imagesList(),
                            SizedBox(
                              height: SizeConfig.safeBlockAverage * 30,
                            ),
                          ],
                        ),
                      ),
                    );
                  } else if (state is ResultOfAddOfferState) {
                    // if success - clear data
                    _commentController.text = '';
                    _priceController.text = '';
                    _addressController.text = '';
                    images = List();
                    return Container(
                      height: SizeConfig.safeBlockVertical * 60,
                      child: Card(
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'Ваше объявление было отправлено на модерацию и после проверки появится в общем списке объявлений',
                                maxLines: 6,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: SizeConfig.safeBlockAverage * 3,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: FlatButton(
                                  color: Colors.grey[300],
                                  child: Text(
                                    'Посмотреть ваше объявление',
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: SizeConfig.safeBlockAverage * 3,
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              OfferDetailsSingle(
                                            offer: state.offer,
                                          ),
                                        ));
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RaisedButton(
                                  child: Text(
                                    'Написать еще одно объявление',
                                    maxLines: 2,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: SizeConfig.safeBlockAverage * 3,
                                    ),
                                  ),
                                  onPressed: () {
                                    _addOfferBloc
                                        .dispatch(DoNotAddOfferEvent());
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  } else if (state is ErrorOfAddOfferState) {
                    return Container(
                      height: SizeConfig.safeBlockVertical * 100,
                      child: Card(
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'Произошла ошибка: ${state.error.toString()}',
                                maxLines: 6,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: SizeConfig.safeBlockAverage * 3,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RaisedButton(
                                  child: Text(
                                    'Попробовать снова',
                                    maxLines: 2,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: SizeConfig.safeBlockAverage * 3,
                                    ),
                                  ),
                                  onPressed: () {
                                    _addOfferBloc
                                        .dispatch(DoNotAddOfferEvent());
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  } else if (state is LoadingAddOfferState) {
                    return Container(
                      height: SizeConfig.safeBlockVertical * 100,
                      child: Center(
                        child: NestProgressIndicator(),
                      ),
                    );
                  } else {
                    return Container(
                      height: SizeConfig.safeBlockVertical * 60,
                      child: Card(
                        child: Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'Произошла ошибка',
                                maxLines: 6,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: SizeConfig.safeBlockAverage * 3,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RaisedButton(
                                  child: Text(
                                    'Попробовать снова',
                                    maxLines: 2,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: SizeConfig.safeBlockAverage * 3,
                                    ),
                                  ),
                                  onPressed: () {
                                    _addOfferBloc
                                        .dispatch(DoNotAddOfferEvent());
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }
}
