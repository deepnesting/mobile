import 'dart:io';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:deepnesting/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

class PersonalWindow extends StatefulWidget {
  final String firstName;
  final String lastName;
  final DateTime birthday;
  final String homeTown;
  final String userBio;

  const PersonalWindow({
    Key key,
    @required this.firstName,
    @required this.lastName,
    @required this.birthday,
    @required this.homeTown,
    @required this.userBio,
  }) : super(key: key);
  _PersonalWindowState createState() => _PersonalWindowState();
}

class _PersonalWindowState extends State<PersonalWindow> {
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _homeTownController = TextEditingController();
  final _userBioController = TextEditingController();
  PersonalBloc _personalBloc;
  LoginBloc _loginBloc;
  UserOffersBloc _userOffersBloc;
  FavoriteOffersBloc _favoriteOffersBloc;
  bool _showMyOffers;
  bool _showFavoriteOffers;
  File personalPhoto;
  int _fieldTimer;
  int _fieldTimerValidation;
  String avatarUrl;
  DateTime _birthdayDate;

  @override
  void initState() {
    super.initState();
    _personalBloc = BlocProvider.of<PersonalBloc>(context);
    _personalBloc.dispatch(ShowPersonalEvent());
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _userOffersBloc = BlocProvider.of<UserOffersBloc>(context);
    _favoriteOffersBloc = BlocProvider.of<FavoriteOffersBloc>(context);
    _firstNameController.text = widget.firstName;
    _lastNameController.text = widget.lastName;
    _homeTownController.text = widget.homeTown;
    _userBioController.text = widget.userBio;
    _firstNameController.addListener(_changeFirstName);
    _lastNameController.addListener(_changeLastName);
    _homeTownController.addListener(_changeHomeTown);
    _userBioController.addListener(_changeUserInfo);
    _showMyOffers = false;
    _showFavoriteOffers = false;
    _fieldTimer = _fieldTimerValidation = 0;
    _birthdayDate = widget.birthday;
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    super.dispose();
  }

  chooseDate() async {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
    );
  }

  _changeFirstName() {
    setState(() {
      _fieldTimer++;
    });
    Future.delayed(const Duration(milliseconds: 1000), () {
      setState(() {
        _fieldTimerValidation++;
      });
      if (_fieldTimer == _fieldTimerValidation &&
          _firstNameController.text != widget.firstName) {
        // null timers
        _fieldTimer = _fieldTimerValidation = 0;
        _personalBloc.dispatch(
          UpdatePersonalEvent(
            firstName: _firstNameController.text,
            lastName: _lastNameController.text,
            avatar: avatarUrl,
            userPhoto: personalPhoto,
            birthday: _birthdayDate,
            hometown: _homeTownController.text,
            userBio: _userBioController.text,
          ),
        );
      }
    });
  }

  _changeLastName() {
    setState(() {
      _fieldTimer++;
    });
    Future.delayed(const Duration(milliseconds: 1000), () {
      setState(() {
        _fieldTimerValidation++;
      });
      if (_fieldTimer == _fieldTimerValidation &&
          _lastNameController.text != widget.lastName) {
        // null timers
        _fieldTimer = _fieldTimerValidation = 0;
        _personalBloc.dispatch(
          UpdatePersonalEvent(
            firstName: _firstNameController.text,
            lastName: _lastNameController.text,
            avatar: avatarUrl,
            userPhoto: personalPhoto,
            birthday: _birthdayDate,
            hometown: _homeTownController.text,
            userBio: _userBioController.text,
          ),
        );
      }
    });
  }

  _changeHomeTown() {
    setState(() {
      _fieldTimer++;
    });
    Future.delayed(const Duration(milliseconds: 1000), () {
      setState(() {
        _fieldTimerValidation++;
      });
      if (_fieldTimer == _fieldTimerValidation &&
          _homeTownController.text != widget.homeTown) {
        // null timers
        _fieldTimer = _fieldTimerValidation = 0;
        _personalBloc.dispatch(
          UpdatePersonalEvent(
            firstName: _firstNameController.text,
            lastName: _lastNameController.text,
            avatar: avatarUrl,
            userPhoto: personalPhoto,
            birthday: _birthdayDate,
            hometown: _homeTownController.text,
            userBio: _userBioController.text,
          ),
        );
      }
    });
  }

  _changeUserInfo() {
    setState(() {
      _fieldTimer++;
    });
    Future.delayed(
      const Duration(milliseconds: 1000),
      () {
        setState(() {
          _fieldTimerValidation++;
        });
        if (_fieldTimer == _fieldTimerValidation &&
            _userBioController.text != widget.userBio) {
          // null timers
          _fieldTimer = _fieldTimerValidation = 0;
          _personalBloc.dispatch(
            UpdatePersonalEvent(
              firstName: _firstNameController.text,
              lastName: _lastNameController.text,
              avatar: avatarUrl,
              userPhoto: personalPhoto,
              birthday: _birthdayDate,
              hometown: _homeTownController.text,
              userBio: _userBioController.text,
            ),
          );
        }
      },
    );
  }

  Future<File> getPersonalPhoto({@required String imageUrl}) async {
    return await WidgetUtils.imageCached(url: imageUrl);
  }

  Widget _myOffersButton() {
    return _showMyOffers
        ? InkWell(
            onTap: () {
              setState(() {
                _showMyOffers = false;
              });
            },
            child: Card(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    'Список поданных объявлений',
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: SizeConfig.safeBlockAverage * 3,
                    ),
                  ),
                ),
              ),
            ),
          )
        : InkWell(
            onTap: () {
              setState(() {
                _showMyOffers = true;
              });
            },
            child: Card(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    'Показать мои объявления',
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: SizeConfig.safeBlockAverage * 3,
                    ),
                  ),
                ),
              ),
            ),
          );
  }

  Widget _favoriteButton() {
    return _showFavoriteOffers
        ? InkWell(
            onTap: () {
              _favoriteOffersBloc.dispatch(ClearFavoriteOffersEvent());
              setState(() {
                _showFavoriteOffers = false;
              });
            },
            child: Card(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    'Список избранных объявлений',
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: SizeConfig.safeBlockAverage * 3,
                    ),
                  ),
                ),
              ),
            ),
          )
        : InkWell(
            onTap: () {
              _favoriteOffersBloc.dispatch(FetchFavoriteOffersEvent());
              setState(() {
                _showFavoriteOffers = true;
              });
            },
            child: Card(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    'Показать избранные объявления',
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: SizeConfig.safeBlockAverage * 3,
                    ),
                  ),
                ),
              ),
            ),
          );
  }

  Widget _myOffers() {
    return BlocBuilder(
      bloc: _userOffersBloc,
      builder: (BuildContext context, UserOffersState state) {
        if (state is ListOfUserOffersState) {
          List<OfferModel> _userOffers = state.userOffers;
          return ListView.builder(
            shrinkWrap: true,
            physics: const ClampingScrollPhysics(),
            itemCount: _userOffers.length,
            itemBuilder: (BuildContext context, int index) {
              final OfferModel offer = _userOffers[index];
              return OfferCard(
                offer: offer,
              );
            },
          );
        } else if (state is ErrorUserOffersState) {
          return Container(
            height: SizeConfig.safeBlockVertical * 100,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    state.error.toString(),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }

  Widget _favoriteOffers() {
    return BlocBuilder(
      bloc: _favoriteOffersBloc,
      builder: (BuildContext context, FavoriteOffersState state) {
        if (state is ListOfFavoriteOffersState) {
          List<OfferModel> _favoriteOffers = state.favoriteOffers;
          if (_favoriteOffers != null && _favoriteOffers.length != 0) {
            return ListView.builder(
                shrinkWrap: true,
                physics: const ClampingScrollPhysics(),
                itemCount: _favoriteOffers.length,
                itemBuilder: (BuildContext context, int index) {
                  final OfferModel offer = _favoriteOffers[index];
                  return OfferCard(
                    offer: offer,
                    forceRefresh: true,
                  );
                });
          } else {
            return Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Избранных объявлений нет',
                  style: TextStyle(
                    fontSize: SizeConfig.safeBlockAverage * 3,
                  ),
                ),
              ),
            );
          }
        } else if (state is ErrorFavoriteOffersState) {
          return Container(
            height: SizeConfig.safeBlockVertical * 100,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    state.error.toString(),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }

  Widget avatarButton({@required String imageUrl}) {
    if (personalPhoto == null) {
      return InkWell(
        onTap: () {
          getImage();
        },
        child: Container(
          width: SizeConfig.safeBlockAverage * 40,
          child: FutureBuilder(
            future: WidgetUtils.imageCached(
              url: imageUrl,
            ),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return Image.file(
                  snapshot.data,
                );
              } else {
                return Image.asset(
                  'assets/logo/nestLogo.jpg',
                );
              }
            },
          ),
        ),
      );
    } else {
      return InkWell(
        onTap: () {
          getImage();
        },
        child: Container(
          width: SizeConfig.safeBlockAverage * 40,
          child: Image.file(
            personalPhoto,
            fit: BoxFit.cover,
          ),
        ),
      );
    }
  }

  Widget birthdayField() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Дата рождения',
                style: TextStyle(
                  fontSize: 12.5,
                  color: Colors.grey[600],
                ),
              ),
            ],
          ),
        ),
        DateTimeField(
          format: DateFormat("dd/mm/yyyy"),
          onShowPicker: (context, currentValue) {
            return showDatePicker(
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100),
            );
          },
          autovalidate: true,
          validator: (date) =>
              date == null ? 'Введите, пожалуйста, дату рождения' : null,
          onChanged: (date) {
            setState(() {
              _birthdayDate = date;
            });
            _personalBloc.dispatch(
              UpdatePersonalEvent(
                firstName: _firstNameController.text,
                lastName: _lastNameController.text,
                avatar: avatarUrl,
                userPhoto: personalPhoto,
                birthday: _birthdayDate,
                hometown: _homeTownController.text,
                userBio: _userBioController.text,
              ),
            );
          },
          onSaved: (date) {
            setState(() {
              _birthdayDate = date;
            });

            _personalBloc.dispatch(
              UpdatePersonalEvent(
                firstName: _firstNameController.text,
                lastName: _lastNameController.text,
                avatar: avatarUrl,
                userPhoto: personalPhoto,
                birthday: _birthdayDate,
                hometown: _homeTownController.text,
                userBio: _userBioController.text,
              ),
            );
          },
        ),
      ],
    );
  }

  Future getImage() async {
    /*File tempImage = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (tempImage != null) {
      setState(() {
        personalPhoto = tempImage;
      });
    }*/
    File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);

    File croppedFile = await ImageCropper.cropImage(
      sourcePath: imageFile.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
      ],
      androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'Cropper',
          toolbarColor: Colors.white,
          toolbarWidgetColor: nestCyan,
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false),
    );
    if (croppedFile != null) {
      setState(() {
        personalPhoto = croppedFile;
      });

      _personalBloc.dispatch(UpdatePersonalEvent(
        firstName: _firstNameController.text,
        lastName: _lastNameController.text,
        avatar: avatarUrl,
        userPhoto: personalPhoto,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: _loginBloc,
      builder: (BuildContext context, LoginState state) {
        if (state is LogInState) {
          return Container(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15.0, vertical: 5.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          FlatButton(
                            child: Text('Выйти'),
                            color: Colors.grey[300],
                            onPressed: () {
                              _loginBloc.dispatch(LogOutEvent());
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                  Card(
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          'Личный кабинет',
                          maxLines: 2,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: SizeConfig.safeBlockAverage * 3,
                          ),
                        ),
                      ),
                    ),
                  ),
                  BlocBuilder(
                    bloc: _personalBloc,
                    builder: (BuildContext context, PersonalState state) {
                      if (state is InfoPersonalState) {
                        avatarUrl = state.userMe.avatarUrl;
                        return Card(
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                avatarButton(
                                  imageUrl: state.userMe.avatarUrl,
                                ),
                                TextField(
                                  controller: _firstNameController,
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(labelText: 'Имя'),
                                ),
                                TextField(
                                  controller: _lastNameController,
                                  keyboardType: TextInputType.visiblePassword,
                                  decoration:
                                      InputDecoration(labelText: 'Фамилия'),
                                ),
                                birthdayField(),
                                TextField(
                                  controller: _homeTownController,
                                  decoration: InputDecoration(
                                      labelText: 'Родной город'),
                                ),
                                TextField(
                                  controller: _userBioController,
                                  decoration: InputDecoration(
                                      labelText: 'О пользователе'),
                                ),

                                /*Padding(
                                  padding: const EdgeInsets.only(top: 15.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      RaisedButton(
                                        child: Text(
                                          'Обновить данные',
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                        onPressed: () {
                                          _personalBloc
                                              .dispatch(UpdatePersonalEvent(
                                            firstName:
                                                _firstNameController.text,
                                            lastName: _lastNameController.text,
                                            avatar: avatarUrl,
                                            userPhoto: personalPhoto,
                                          ));
                                        },
                                      ),
                                    ],
                                  ),
                                )*/
                              ],
                            ),
                          ),
                        );
                      } else if (state is ErrorPersonalState) {
                        _firstNameController.text = '';
                        _lastNameController.text = '';
                        return Container(
                          height: SizeConfig.safeBlockVertical * 100,
                          child: Center(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text(
                                  state.error,
                                ),
                                RaisedButton(
                                  child: Text(
                                    'Назад',
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                  onPressed: () {
                                    _loginBloc.dispatch(LogOutEvent());
                                  },
                                )
                              ],
                            ),
                          ),
                        );
                      } else {
                        _firstNameController.text = '';
                        _lastNameController.text = '';
                        return Container(
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      }
                    },
                  ),
                  _myOffersButton(),
                  _showMyOffers ? _myOffers() : Container(),
                  _favoriteButton(),
                  _showFavoriteOffers
                      ? BlocBuilder(
                          bloc: _favoriteOffersBloc,
                          builder: (BuildContext context,
                              FavoriteOffersState state) {
                            if (state is ListOfFavoriteOffersState) {
                              List<OfferModel> _favoriteOffers =
                                  state.favoriteOffers;
                              if (_favoriteOffers != null &&
                                  _favoriteOffers.length != 0) {
                                return ListView.builder(
                                    shrinkWrap: true,
                                    physics: const ClampingScrollPhysics(),
                                    itemCount: _favoriteOffers.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      final OfferModel offer =
                                          _favoriteOffers[index];
                                      return OfferCard(
                                        offer: offer,
                                        forceRefresh: true,
                                      );
                                    });
                              } else {
                                return Center(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      'Избранных объявлений нет',
                                      style: TextStyle(
                                        fontSize:
                                            SizeConfig.safeBlockAverage * 3,
                                      ),
                                    ),
                                  ),
                                );
                              }
                            } else if (state is ErrorFavoriteOffersState) {
                              return Container(
                                height: SizeConfig.safeBlockVertical * 100,
                                child: Center(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Text(
                                        state.error.toString(),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            } else {
                              return Container(
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                              );
                            }
                          },
                        )
                      : Container(),
                  SizedBox(
                    height: SizeConfig.safeBlockAverage * 50,
                  )
                ],
              ),
            ),
          );
        } else if (state is LogOutState) {
          _firstNameController.text = '';
          _lastNameController.text = '';
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else if (state is ErrorLoginState) {
          _firstNameController.text = '';
          _lastNameController.text = '';
          return Container(
            height: SizeConfig.safeBlockVertical * 100,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    state.error,
                  ),
                  RaisedButton(
                    child: Text(
                      'Назад',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () {
                      _loginBloc.dispatch(LogOutEvent());
                    },
                  )
                ],
              ),
            ),
          );
        } else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}
