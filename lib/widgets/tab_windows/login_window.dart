import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginWindow extends StatefulWidget {
  _LoginWindowState createState() => _LoginWindowState();
}

class _LoginWindowState extends State<LoginWindow> {
  LoginBloc _loginBloc;
  final _loginController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
  }


  @override
  void dispose() {
    _loginController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  Card formTitle() {
    return Card(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text(
            'Введите ваш логин и пароль',
            maxLines: 2,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: SizeConfig.safeBlockAverage * 3,
            ),
          ),
        ),
      ),
    );
  }

  Card loginForm() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: <Widget>[
            TextField(
              controller: _loginController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(labelText: 'email / номер телефона / vkontakte'),
            ),
            TextField(
              controller: _passwordController,
              keyboardType: TextInputType.visiblePassword,
              decoration: InputDecoration(labelText: 'пароль'),
              obscureText: true,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    color: Colors.grey[300],
                    child: Text(
                      'Зарегистрироваться',
                    ),
                    onPressed: () {
                      _loginBloc.dispatch(GoToRegisterWindowLoginEvent());
                    },
                  ),
                  RaisedButton(
                    child: Text(
                      'Войти',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () {
                      _loginBloc.dispatch(EnterLoginEvent(
                        login: _loginController.text,
                        password: _passwordController.text,
                      ));
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            formTitle(),
            loginForm(),
          ],
        ),
      ),
    );
  }
}