import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FloatingButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    OffersBloc _offersBloc = BlocProvider.of<OffersBloc>(context);
    int priceFrom;
    int priceTo;
    int searchType;
    int buildType;
    bool allowPush = false;
    return Container(
      child: BlocBuilder(
        bloc: _offersBloc,
        builder: (BuildContext context, OffersState state) {
          if (state is LoadedOffersState) {
            priceFrom = state.priceFrom;
            priceTo = state.priceTo;
            searchType = state.searchType;
            buildType = state.buildType;
            allowPush = true;
          } else {
            allowPush = false;
          }
          return Stack(
            children: <Widget>[
              FloatingMapButton(
                icon: FontAwesomeIcons.filter,
                onPressed: () {
                  if (allowPush) {
                    showDialog(
                        context: context,
                        builder: (_) => FiltersWindow(
                          priceFrom: priceFrom,
                          priceTo: priceTo,
                          searchType: searchType,
                          buildType: buildType,
                        ));
                  }
                },
              ),
              allowPush
                  ? Container()
                  : Positioned.fill(
                      child: Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.grey.withOpacity(0.5),
                        ),
                        child: Center(
                          child: NestProgressIndicator(),
                        ),
                      ),
                    ),
            ],
          );
        },
      ),
    );
  }
}
