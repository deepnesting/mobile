import 'package:deepnesting/utils/utils.dart';
import 'package:flutter/material.dart';

class FloatingMapButton extends StatelessWidget {
  final IconData icon;
  final void Function() onPressed;

  const FloatingMapButton({Key key, this.icon, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onPressed();
      },
      child: Container(
        width: 65.0,
        height: 65.0,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50.0),
          ),
          elevation: 8,
          child: Icon(
            icon,
            color: nestGrayIcons,
          ),
        ),
      ),
    );
  }
}
