import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:deepnesting/widgets/widgets.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:simple_moment/simple_moment.dart';

class OfferDetailsSingle extends StatefulWidget {
  final OfferSingleModel offer;

  const OfferDetailsSingle({Key key, @required this.offer}) : super(key: key);

  _OfferDetailsSingleState createState() => _OfferDetailsSingleState();
}

class _OfferDetailsSingleState extends State<OfferDetailsSingle> {
  bool showContacts;
  OfferContactsBloc _offerContactsBloc;

  @override
  void initState() {
    super.initState();
    showContacts = false;
    _offerContactsBloc = BlocProvider.of<OfferContactsBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    List<String> images =
    WidgetUtils.getFullSizeImagesFromImageModel(images: widget.offer.images);
    Moment moment = Moment.now();
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.offer.title,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 15.0),
                      child: Container(
                        width: SizeConfig.safeBlockAverage * 9,
                        child: FutureBuilder(
                          future: WidgetUtils.imageCached(
                              url: widget.offer.user.avatarUrl),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              return Image.file(
                                snapshot.data,
                              );
                            } else {
                              return Image.asset(
                                'assets/noAvatar.jpg',
                              );
                            }
                          },
                        ),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          '${widget.offer.user.firstName} ${widget.offer.user.lastName}',
                          maxLines: 2,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: SizeConfig.safeBlockAverage * 3,
                          ),
                        ),
                        Text(
                          widget.offer.title,
                          maxLines: 5,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: SizeConfig.safeBlockAverage * 3,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      widget.offer.price != null
                          ? Flexible(
                        flex: 1,
                        child: widget.offer.price != 0
                            ? Text(
                          'Цена: ${widget.offer.price}',
                          maxLines: 2,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize:
                            SizeConfig.safeBlockAverage * 3,
                          ),
                        )
                            : Container(),
                      )
                          : Container(),
                      Flexible(
                        flex: 2,
                        child: Text(
                          'Размещено: ${moment.from(DateTime.parse(widget.offer.publishedAt))}',
                          maxLines: 2,
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            fontSize: SizeConfig.safeBlockAverage * 3,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: SizeConfig.safeBlockHorizontal * 100,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15.0),
                    child: Text(
                      widget.offer.comment,
                      style: TextStyle(
                        fontSize: SizeConfig.safeBlockAverage * 3,
                      ),
                    ),
                  ),
                ),
                ButtonBar(
                  children: <Widget>[
                    showContacts
                        ? FlatButton(
                      child: Text(
                        'Скрыть контакты',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: SizeConfig.safeBlockAverage * 3,
                        ),
                      ),
                      color: Colors.grey[200],
                      onPressed: () {
                        setState(() {
                          showContacts = false;
                        });
                      },
                    )
                        : RaisedButton(
                      child: Text(
                        'Показать контакты',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: SizeConfig.safeBlockAverage * 3),
                      ),
                      onPressed: () {
                        setState(() {
                          _offerContactsBloc.dispatch(ClearContactsEvent());
                          _offerContactsBloc.dispatch(LoadContactsEvent(offerId: widget.offer.id));
                          showContacts = true;
                        });
                      },
                    ),
                  ],
                ),
                showContacts
                    ? BlocBuilder(
                  bloc: _offerContactsBloc,
                  builder:
                      (BuildContext context, OfferContactsState state) {
                    if (state is LoadedContactsState) {
                      return Container(
                        width: SizeConfig.safeBlockHorizontal * 100,
                        child: Padding(
                          padding:
                          const EdgeInsets.symmetric(vertical: 10.0),
                          child: ListView.builder(
                              shrinkWrap: true,
                              physics: const ClampingScrollPhysics(),
                              itemCount: state.contactsList.length,
                              itemBuilder:
                                  (BuildContext context, int index) {
                                OfferContactsModel contact =
                                state.contactsList[index];
                                bool ifLink = contact.type == 'link';
                                return RichText(
                                    text: TextSpan(children: [
                                      TextSpan(
                                          text: contact.text,
                                          style: TextStyle(
                                              fontSize: SizeConfig
                                                  .safeBlockAverage *
                                                  3,
                                              color: ifLink
                                                  ? Colors.blue
                                                  : Colors.black),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              if (ifLink) {
                                                WidgetUtils.redirectToUrl(contact.value);
                                              }
                                            }),
                                    ]));
                              }),
                        ),
                      );
                    } else {
                      return NestProgressIndicator();
                    }
                  },
                )
                    : Container(),
                SizedBox(
                  height: SizeConfig.safeBlockAverage * 5,
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    images.length != 1
                        ? StaggeredGridView.countBuilder(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      crossAxisCount: 4,
                      itemCount: images.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      HeroPhotoViewWrapper(
                                        firstPage: index,
                                        imageList: images,
                                      ),
                                ));
                          },
                          child: Container(
                            child: Hero(
                              tag:
                              'image${widget.offer.user.avatarUrl}$index',
                              child: FutureBuilder(
                                future: WidgetUtils.imageCached(
                                    url: images[index]),
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  if (snapshot.hasData) {
                                    return Image.file(
                                      snapshot.data,
                                    );
                                  } else {
                                    return Image.asset(
                                      'assets/noPhoto.jpeg',
                                    );
                                  }
                                },
                              ),
                            ),
                          ),
                        );
                      },
                      staggeredTileBuilder: (int index) =>
                          StaggeredTile.fit(2),
                      mainAxisSpacing: 4.0,
                      crossAxisSpacing: 4.0,
                    )
                        : Container(
                      width: SizeConfig.safeBlockHorizontal * 100,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    HeroPhotoViewWrapper(
                                      firstPage: 0,
                                      imageList: images,
                                    ),
                              ));
                        },
                        child: Container(
                          child: Hero(
                            tag: 'image${widget.offer.user.avatarUrl}0',
                            child: FutureBuilder(
                              future:
                              WidgetUtils.imageCached(url: images[0]),
                              builder: (BuildContext context,
                                  AsyncSnapshot snapshot) {
                                if (snapshot.hasData) {
                                  return Image.file(
                                    snapshot.data,
                                  );
                                } else {
                                  return Image.asset(
                                    'assets/noPhoto.jpeg',
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
