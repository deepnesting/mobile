import 'package:deepnesting/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:deepnesting/widgets/widgets.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:random_string/random_string.dart';

class OfferImages extends StatelessWidget {
  final List<String> images;

  const OfferImages({
    Key key,
    @required this.images,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            images.length != 1
                ? StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    crossAxisCount: SizeConfig.ifVertical ? 4 : 6,
                    itemCount: SizeConfig.ifVertical
                        ? images.length > 6 ? 6 : images.length
                        : images.length > 9 ? 9 : images.length,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => HeroPhotoViewWrapper(
                                firstPage: index,
                                imageList: images,
                              ),
                            ),
                          );
                        },
                        child: Container(
                          child: Hero(
                            tag: 'image${randomString(10)}${images[index]}',
                            child: Stack(
                              children: <Widget>[
                                FadeInImage.assetNetwork(
                                  placeholder: 'assets/noPhoto.jpeg',
                                  image: images[index],
                                ),
                                ImageBubble(
                                  length: images.length,
                                  index: index,
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                    staggeredTileBuilder: (int index) => StaggeredTile.fit(
                        SizeConfig.ifVertical
                            ? (index < 2 ? 2 : 1)
                            : (index < 3 ? 2 : 1)),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                  )
                : Container(
                    width: SizeConfig.safeBlockHorizontal * 100,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HeroPhotoViewWrapper(
                              firstPage: 0,
                              imageList: images,
                            ),
                          ),
                        );
                      },
                      child: Container(
                        child: Hero(
                          tag: 'image${randomString(10)}0',
                          child: FutureBuilder(
                            future: WidgetUtils.imageCached(url: images[0]),
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (snapshot.hasData) {
                                return Image.file(
                                  snapshot.data,
                                  fit: BoxFit.fill,
                                );
                              } else {
                                return Container(
                                  decoration: BoxDecoration(color: Colors.red),
                                  child: Image.asset(
                                    'assets/noPhoto.jpeg',
                                    fit: BoxFit.fill,
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ],
    );
  }
}
