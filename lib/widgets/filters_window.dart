import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;
import './toggle_buttons2.dart';

class FiltersWindow extends StatefulWidget {
  final int priceFrom;
  final int priceTo;
  final int searchType;
  final int buildType;

  const FiltersWindow(
      {Key key, this.priceFrom, this.priceTo, this.searchType, this.buildType})
      : super(key: key);

  _FiltersWindowState createState() => _FiltersWindowState();
}

class _FiltersWindowState extends State<FiltersWindow> {
  int _nestSearchType;
  int _nestBuildType;
  double _nestPriceFrom;
  double _nestPriceTo;
  List<bool> searchTypeSelected = [false, false];
  List<bool> buildTypeSelected = [false, false];
  List<int> searchTypeOptions = [1, 2];
  List<int> buildTypeOptions = [0, 1];

  @override
  void initState() {
    super.initState();
    _nestSearchType = widget.searchType;
    _nestBuildType = widget.buildType;
    _nestPriceFrom =
        widget.priceFrom == null ? 0.0 : widget.priceFrom.toDouble();
    _nestPriceTo = widget.priceTo == null ? 40000.0 : widget.priceTo.toDouble();

    searchTypeSelected =
        searchTypeOptions.map((option) => option == widget.searchType).toList();
    buildTypeSelected =
        buildTypeOptions.map((option) => option == widget.buildType).toList();
  }

  @override
  Widget build(BuildContext context) {
    OffersBloc _offersBloc = BlocProvider.of<OffersBloc>(context);
    return AlertDialog(
      title: Text('Настройки поиска'),
      contentPadding: const EdgeInsets.all(10.0),
      content: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              height: SizeConfig.safeBlockAverage * 5,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Container(
                width: SizeConfig.safeBlockHorizontal * 80,
                child: Text(
                  'Бюджет от ${_nestPriceFrom.toInt()} руб. до ${_nestPriceTo.toInt()} руб.',
                  textAlign: TextAlign.start,
                  maxLines: 2,
                ),
              ),
            ),
            SliderTheme(
              data: SliderTheme.of(context).copyWith(
                activeTrackColor: nestCyanButtonText,
                inactiveTrackColor: nestCyanLight,
                valueIndicatorColor: nestCyanButtonText,
                thumbColor: nestCyanButtonText,
                overlayColor: nestCyanLight,
                showValueIndicator: ShowValueIndicator.always,
              ),
              child: frs.RangeSlider(
                  min: 0.0,
                  max: 40000.0,
                  lowerValue: _nestPriceFrom,
                  upperValue: _nestPriceTo,
                  divisions: 40,
                  showValueIndicator: true,
                  valueIndicatorMaxDecimals: 1,
                  valueIndicatorFormatter: (int index, double value) {
                    return '$value руб.';
                  },
                  onChanged: (double newLowerValue, double newUpperValue) {
                    setState(() {
                      _nestPriceFrom = newLowerValue;
                      _nestPriceTo = newUpperValue;
                    });
                  },
                  onChangeEnd: (double newLowerValue, double newUpperValue) {
                    _offersBloc.dispatch(ClearOffersEvent());
                    _offersBloc.dispatch(LoadOffersEvent(
                      priceFrom: newLowerValue.toInt(),
                      priceTo: newUpperValue.toInt(),
                      searchType: _nestSearchType,
                      buildType: _nestBuildType,
                    ));
                  }),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Container(
                width: SizeConfig.safeBlockHorizontal * 80,
                child: Text(
                  'Что ищем?',
                  textAlign: TextAlign.start,
                ),
              ),
            ),
            ToggleButtons2(
              onPressed: (int index) {
                setState(() {
                  for (int buttonIndex = 0;
                  buttonIndex < searchTypeSelected.length;
                  buttonIndex++) {
                    if (buttonIndex == index) {
                      searchTypeSelected[buttonIndex] = true;
                      _nestSearchType = index+1;
                      _offersBloc.dispatch(ClearOffersEvent());
                      _offersBloc.dispatch(LoadOffersEvent(
                        priceFrom: _nestPriceFrom.toInt(),
                        priceTo: _nestPriceTo.toInt(),
                        searchType: _nestSearchType,
                        buildType: _nestBuildType,
                      ));
                    } else {
                      searchTypeSelected[buttonIndex] = false;
                    }
                  }
                });
              },
              items: ["Гнездоискателя", "Гнездышко"],
              isSelected: searchTypeSelected,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Container(
                width: SizeConfig.safeBlockHorizontal * 80,
                child: Text(
                  'Тип жилья',
                  textAlign: TextAlign.start,
                ),
              ),
            ),
            ToggleButtons2(
              onPressed: (int index) {
                setState(() {
                  for (int buttonIndex = 0;
                      buttonIndex < buildTypeSelected.length;
                      buttonIndex++) {
                    if (buttonIndex == index) {
                      buildTypeSelected[buttonIndex] = true;
                      _nestBuildType = index;
                      _offersBloc.dispatch(ClearOffersEvent());
                      _offersBloc.dispatch(LoadOffersEvent(
                        priceFrom: _nestPriceFrom.toInt(),
                        priceTo: _nestPriceTo.toInt(),
                        searchType: _nestSearchType,
                        buildType: _nestBuildType,
                      ));
                    } else {
                      buildTypeSelected[buttonIndex] = false;
                    }
                  }
                });
              },
              items: ["Комнату", "Квартиру"],
              isSelected: buildTypeSelected,
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: Text(
                    'Отмена',
                    style: TextStyle(
                        color: nestGrayButtonText,
                        fontSize: SizeConfig.safeBlockAverage * 3),
                  ),
                  onPressed: () {
                    _offersBloc.dispatch(ClearOffersEvent());
                    _offersBloc.dispatch(LoadOffersEvent(
                      priceFrom: null,
                      priceTo: null,
                      searchType: null,
                      buildType: null,
                    ));
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text(
                    'Применить',
                    style: TextStyle(
                        color: nestCyanButtonText,
                        fontSize: SizeConfig.safeBlockAverage * 3),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
