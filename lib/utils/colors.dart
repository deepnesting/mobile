import 'package:flutter/material.dart';

const nestMainColor = const Color(0xFF038992);
const nestOrange = const Color(0xFFFF5722);
const nestGray = const Color(0xFFFBFBFB);
const nestDarkGray = const Color(0xFFC4C4C4);
const nestCyan = const Color(0xFF5EDFDE);
const nestCyanButtonText = const Color(0xFF28C9C8);
const nestCyanLight = const Color(0xFF94FFFF);
const nestBackground = const Color(0xFFEBEBEB);
const nestGrayIcons = const Color(0xFF212121);
const nestGrayBackgroundButtons = const Color(0xFFEEEEEE);
const nestGrayButtonText = const Color(0xFFBDBDBD);
