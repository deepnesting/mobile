import 'package:simple_moment/simple_moment.dart';

class LocaleRu implements ILocaleData {
  String get seconds => 'пару секунд назад';

  String get aMinute => 'минуту';
  String get minutes => '%i минут';

  String get anHour => 'час';
  String get hours => '%i часов';

  String get aDay => 'день';
  String get days => '%i дня';

  String get aMonth => 'месяц';
  String get months => '%i месяцев';

  String get aYear => 'год';
  String get years => '%i лет';

  String get futureIdentifier => 'в течение';
  String get pastIdentifier => 'назад';

  IdentifierPosition get futurePosition => IdentifierPosition.prepend;
  IdentifierPosition get pastPosition => IdentifierPosition.append;
}
