import 'package:deepnesting/utils/utils.dart';
import 'package:flutter/material.dart';

class NestTheme {
  static get theme {
    return ThemeData(
        primaryColor: nestGray,
        buttonColor: nestCyan,
        pageTransitionsTheme: PageTransitionsTheme());
  }
}
