import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesUtils {
  /// save variable to shared preferences
  static setVariableToShared(
      {Key key, @required String variable, @required String title}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString(
        title, (variable != null && variable != '') ? variable : "");
  }

  /// get variable from shared variables
  static Future<String> getVariableFromShared(
      {Key key, @required String title}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String result = preferences.getString(title);
    return result;
  }

  /// save int variables to shared preferences
  static setIntVariableToShared(
      {Key key, @required int variable, @required String title}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt(title, variable);
  }

  /// get int variable from shared variables
  static Future<int> getIntVariableFromShared(
      {Key key, @required String title}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int result = preferences.getInt(title);
    return result;
  }

  /// save int variables to shared preferences
  static setDoubleVariableToShared(
      {Key key, @required double variable, @required String title}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setDouble(title, variable);
  }

  /// get int variable from shared variables
  static Future<double> getDoubleVariableFromShared(
      {Key key, @required String title}) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    double result = preferences.getDouble(title);
    return result;
  }
}
