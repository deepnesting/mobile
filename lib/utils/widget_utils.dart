import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:ui' as ui;

import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/repository/nest_db.dart';
import 'package:deepnesting/utils/size_config.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image/image.dart' as images;
import 'package:url_launcher/url_launcher.dart';

class WidgetUtils {
  static bool ifVertical() {
    return SizeConfig.safeBlockVertical > SizeConfig.safeBlockHorizontal;
  }

  static imageCached({Key key, @required String url}) async {
    return await DefaultCacheManager().getSingleFile(url);
  }

  static offerImagesList({@required OfferModel offer}) async {
    List<OfferImage> offerImages =
        await OfferImage().select().offerId.equals(offer.id).toList();
    final String url = 'https://m.ugnest.com';
    List<String> images =
        offerImages.map((image) => '$url${image.imageUrl}').toList();
    return images;
  }

  static List<String> getFullSizeImages({@required List<String> images}) {
    return images
        .map<String>((image) => image.contains('&url=')
            ? (image.contains('/images/')
                ? 'https://m.ugnest.com${image.split('&url=')[1]}'
                : image.split('&url=')[1])
            : image)
        .toList();
  }

  static List<String> getFullSizeImagesFromImageModel(
      {@required List<ImageModel> images}) {
    return images.map((image) => 'https://m.ugnest.com${image.url}').toList();
  }

  static String getFullSizeImageFromImageModel({@required ImageModel image}) {
    return 'https://m.ugnest.com${image.url}';
  }

  static String getFullSizeImageURL({@required String imageURL}) {
    if (imageURL.contains('&url=')) {
      String cutImageUrl = imageURL.split('&url=')[1];
      if (cutImageUrl.contains('/images/')) {
        return 'https://m.ugnest.com$cutImageUrl';
      } else {
        return imageURL.split('&url=')[1];
      }
    } else {
      return imageURL;
    }
    //return imageURL.contains('&url=') ? imageURL.split('&url=')[1] : imageURL;
  }

  static String getImageUrlFromImages({@required String imageURL}) {
    if (imageURL.contains('/images/') && imageURL.startsWith('/images/')) {
      return 'https://m.ugnest.com$imageURL';
    } else {
      return imageURL;
    }
  }

  static getImagesWithUrl({@required List<String> images}) {
    return images.map((imageUrl) => 'https://m.ugnest.com$imageUrl').toList();
  }

  static getFullSizeCashedImages({@required List<String> images}) async {
    List<String> fullSizeImages = WidgetUtils.getFullSizeImages(images: images);
    List<File> fileImages = new List();

    for (int i = 0; i < fullSizeImages.length; i++) {
      File fileImage =
          await DefaultCacheManager().getSingleFile(fullSizeImages[i]);
      fileImages.add(fileImage);
    }

    return fileImages;
  }

  Future<images.Image> createImage(
      String imageFile, int width, int height) async {
    ByteData imageData;
    try {
      imageData = await rootBundle.load(imageFile);
    } catch (e) {
      print('caught $e');
      return null;
    }

    if (imageData == null) {
      return null;
    }

    List<int> bytes = Uint8List.view(imageData.buffer);
    var image = images.decodeImage(bytes);

    return images.copyResize(image, width: width, height: height);
  }

  Future<BitmapDescriptor> createBitmapDescriptor(
      {@required String text, @required bool isPoint}) async {
    var child = isPoint
        ? await createImage('assets/markers/marker.png', 150, 88)
        : await createImage('assets/markers/cluster.png', 100, 100);

    if (child == null) {
      return null;
    }

    images.drawString(child, images.arial_48, 0, 0, '$text');

    var resized = images.copyResize(child, width: 100, height: 100);

    var png = images.encodePng(resized);

    return BitmapDescriptor.fromBytes(png);
  }

  static redirectToUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw Exception('Не удалось перейти по адресу $url');
    }
  }

  static Future<BitmapDescriptor> bitmapDescriptor(
      {@required int priceOrNumber,
      @required bool isCluster,
      bool ifCheckedOffer}) async {
    String color = 'fill:#23B250;';
    if (ifCheckedOffer) {
      color = 'fill:#B6B6B6;';
    }
    String svgPointString = '''
      <?xml version="1.0"?>
      <svg width="54" height="33" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <rect x="0" y="0" width="54" height="23" rx="3" ry="3" style="$color" />
      <polygon points="22,23 27,28 32,23" style="$color" />
      <text x="27" y="16" style="text-anchor:middle;font-size:13px;fill:#ffffff;font-family:DejaVu Sans,Verdana,Geneva,sans-serif" >$priceOrNumber₽</text>
      </svg>''';
    String svgClusterString = '''
      <?xml version="1.0"?>
      <svg width="25" height="25"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink">
      <circle cx="12" cy="12" r="12" style="fill:#23B250" />
      <circle cx="12" cy="12" r="8" style="fill:#ffffff" />
      <text x="12" y="16" style="text-anchor:middle;font-size:12px;fill:#000;font-family:DejaVu Sans,Verdana,Geneva,sans-serif" >$priceOrNumber</text>
      </svg>''';
    int markerWidth = isCluster ? 100 : 200;
    int markerHeight = isCluster ? 100 : 100;
    DrawableRoot drawableRoot = await svg.fromSvgString(
        isCluster ? svgClusterString : svgPointString, null);
    ui.Picture picture = drawableRoot.toPicture(
        size: Size(markerWidth.toDouble(), markerHeight.toDouble()));
    ui.Image image = await picture.toImage(markerWidth, markerHeight);
    ByteData bytes = await image.toByteData(format: ui.ImageByteFormat.png);
    return BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
  }

  static Future<BitmapDescriptor> imageBitmapDescriptor(
      {@required String imageUrl, @required int targetWidth}) async {
    final File markerImageFile =
        await DefaultCacheManager().getSingleFile(imageUrl);
    final Uint8List markerImageBytes = await markerImageFile.readAsBytes();

    final Codec markerImageCodec = await instantiateImageCodec(
      markerImageBytes,
      targetWidth: targetWidth,
    );

    final FrameInfo frameInfo = await markerImageCodec.getNextFrame();
    final ByteData byteData = await frameInfo.image.toByteData(
      format: ImageByteFormat.png,
    );

    final Uint8List resizedMarkerImageBytes = byteData.buffer.asUint8List();

    return BitmapDescriptor.fromBytes(resizedMarkerImageBytes);
  }
}
