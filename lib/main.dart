import 'package:bloc/bloc.dart';
import 'package:deepnesting/blocs/blocks.dart';
import 'package:deepnesting/repository/nest_repository.dart';
import 'package:deepnesting/widgets/nest_progress_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_moment/simple_moment.dart';

import 'screens/screens.dart';
import 'utils/utils.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = NestBlocDelegate();
  runApp(NestApp());
}

class NestApp extends StatelessWidget {
  final TabBloc _tabBloc = TabBloc();
  final OffersBloc _offersBloc = OffersBloc(nestRepository: NestRepository());
  final MainPageOffersBloc _mainPageOffersBloc =
      MainPageOffersBloc(nestRepository: NestRepository());
  final OfferContactsBloc _offerContactsBloc =
      OfferContactsBloc(nestRepository: NestRepository());
  final LoginBloc _loginBloc = LoginBloc(nestRepository: NestRepository());
  final AddOfferBloc _addOfferBloc =
      AddOfferBloc(nestRepository: NestRepository());
  final FavoriteOffersBloc _favoriteOffersBloc =
      FavoriteOffersBloc(nestRepository: NestRepository());
  final OfferCommentBloc _offerCommentBloc =
      OfferCommentBloc(nestRepository: NestRepository());
  final SearchTabBloc _searchTabBloc =
      SearchTabBloc(nestRepository: NestRepository());
  final CheckMapBloc _checkMapBloc =
      CheckMapBloc(nestRepository: NestRepository());
  final FavoriteListIntBloc _favoriteListIntBloc =
      FavoriteListIntBloc(nestRepository: NestRepository());

  @override
  Widget build(BuildContext context) {
    _checkMapBloc.dispatch(SendCheckMapEvent());
    Moment.setLocaleGlobally(LocaleRu());
    return MultiBlocProvider(
      providers: <BlocProvider>[
        BlocProvider<TabBloc>(
          builder: (BuildContext context) => _tabBloc,
        ),
        BlocProvider<OffersBloc>(
          builder: (BuildContext context) => _offersBloc,
        ),
        BlocProvider<MainPageOffersBloc>(
          builder: (BuildContext context) => _mainPageOffersBloc,
        ),
        BlocProvider<OfferContactsBloc>(
          builder: (BuildContext context) => _offerContactsBloc,
        ),
        BlocProvider<LoginBloc>(
          builder: (BuildContext context) => _loginBloc,
        ),
        BlocProvider<PersonalBloc>(
          builder: (BuildContext context) => PersonalBloc(
              nestRepository: NestRepository(), loginBloc: _loginBloc),
        ),
        BlocProvider<AddOfferBloc>(
          builder: (BuildContext context) => _addOfferBloc,
        ),
        BlocProvider<UserOffersBloc>(
          builder: (BuildContext context) => UserOffersBloc(
              nestRepository: NestRepository(), loginBloc: _loginBloc),
        ),
        BlocProvider<FavoriteOffersBloc>(
          builder: (BuildContext context) => _favoriteOffersBloc,
        ),
        BlocProvider<OfferCommentBloc>(
          builder: (BuildContext context) => _offerCommentBloc,
        ),
        BlocProvider<SearchTabBloc>(
          builder: (BuildContext context) => _searchTabBloc,
        ),
        BlocProvider<CheckMapBloc>(
          builder: (BuildContext context) => _checkMapBloc,
        ),
        BlocProvider<FavoriteListIntBloc>(
          builder: (BuildContext context) => _favoriteListIntBloc,
        ),
        BlocProvider<ModerationBloc>(
          builder: (BuildContext context) => ModerationBloc(
              nestRepository: NestRepository(), loginBloc: _loginBloc),
        ),
      ],
      child: MaterialApp(
        theme: NestTheme.theme,
        home: BlocBuilder(
          bloc: _checkMapBloc,
          builder: (BuildContext context, CheckMapState state) {
            if (state is SetCheckMapState) {
              return HomeScreen(
                ifMapDisabled: state.ifMapDisabled,
              );
            } else if (state is ErrorCheckMapState) {
              return Scaffold(
                body: Center(
                  child: Text('Произошла ошибка ${state.error}'),
                ),
              );
            } else {
              return Scaffold(
                body: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
