// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.
import 'package:deepnesting/models/models.dart';
import 'package:deepnesting/models/offer_contacts_model.dart';
import 'package:deepnesting/repository/repository.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  test('get offer contacts',() async {
    WebClient webClient = WebClient();

    List<OfferContactsModel> contacts = await webClient.fetchOfferContacts(offerId: 3014);

    for(OfferContactsModel contact in contacts) {
      print(contact.type);
      print(contact.value);
      print(contact.text);
    }
  });

  group('login and register',() {
    test('should return user when login', () async {
      // arrange
      WebClient webClient = WebClient();
      /*String login = 'some@email.com';
      String password = 'Password';*/
      String login = 'wb2@gmail.com';
      String password = 'Parol12';
      // act
      User user = await webClient.login(login: login, password: password);
      print(user.token);
      // assert
      expect(user.token, isNotNull);
    });

    test('should return User when trying to register', () async {
        // arrange
        WebClient webClient = WebClient();
        String login = 'wb2@gmail.com';
        String password = 'Parol12';
        // act
        User user = await webClient.register(login: login, password: password);
        // assert
        print(user.token);
    });

    test('should return something when trying to upload file', () async {
      // arrange
      WebClient webClient = WebClient();
      String login = 'wb2@gmail.com';
      String password = 'Parol12';
      // act
      User user = await webClient.register(login: login, password: password);
      // assert
      print(user.token);
    });
  });
}
